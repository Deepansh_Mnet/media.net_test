<?php

/**
 * 
*/
namespace IncRsvp\Api\Callbacks;

class CustomSettingsCallbacks
{
	public function settingsSectionManager( $args )
	{
		// echo "<h1>Blacklisted</h1>";
	}

	public function settingSanitize( $args )
	{
		$output = get_option( 'rsvp_plugin_post' );
		
		if(isset($_POST['remove_domain'])){
			unset($output['Site-Details']['blacklist'][$_POST['remove_domain']]);
			return $output;
		}
		
		if(isset($_POST['remove_subdomain'])){
			unset($output['Site-Details']['blacklist-subdomain'][$_POST['remove_subdomain']]);
			return $output;
		}

		if(isset($_POST['remove_email'])){
    		unset($output['Site-Details']['blacklist-email'][$_POST['remove_email']]);
			return $output;
		}

		if(isset($_POST['remove_input'])){
    		unset($output['Site-Details']['blacklist-input'][$_POST['remove_input']]);
			return $output;
		}
		
		if(isset($_POST['remove_post'])){
			wp_delete_post( $_POST['remove_post'], true);
			unset($output[$_POST['remove_post']]);
			return $output;
		}
		
		return $output;

	}

	public function textField( $args )
	{
		$name = $args['label_for'];
		$option_name = $args['option_name'];
		$input = get_option( $option_name );
		$value = $input['Site-Details'][$name];
		// echo $value;
		echo '<input type="text" required class="regular-text" id="' . $name . '" name="' . $name . '" placeholder="' . $args['placeholder'] . '" value="'.htmlspecialchars($value).'" >';
	}

	public function textArea( $args )
	{
		$name = $args['label_for'];
		$option_name = $args['option_name'];
		$input = get_option( $option_name );
		$value = $input['Site-Details'][$name];
		// $value = "safa";
		echo '<textarea class="regular-text" id="' . $name . '" name="'. $name . '" placeholder="' . $args['placeholder'] .'" rows="3">'.htmlspecialchars($value).'</textarea>
		';
	}
}