<?php

/**
 * 
*/
namespace IncRsvp\Api\Callbacks;

class PostCallbacks
{
	public function postSectionManager()
	{
		// echo "Create your Custom Post Type<br>";	
	}

	public function postSanitize( $args )
	{
		$output = get_option( 'rsvp_plugin_post' );
		
		if(isset($_POST['remove_post'])){
			wp_delete_post( $_POST['remove_post'], true);
			unset($output[$_POST['remove_post']]);
			return $output;
		}

		return $output;

	}

	public function textField( $args )
	{

		$name = $args['label_for'];
		$option_name = $args['option_name'];
		$input = get_option( $option_name );
		$value = $args['value'];

		if(isset($_POST["edit_post"])){
			$value = $input[$_POST["edit_post"]][$name];
		}

		echo '<input type="text" ' . $args['required'] . ' class="regular-text '. $args['class'] .'" id="' . $name . '" name="' . $name . '" placeholder="' . $args['placeholder'] . '" value="'.htmlspecialchars($value).'" >';
	}

	public function divField( $args )
	{

		$name = $args['label_for'];
		$option_name = $args['option_name'];
		$input = get_option( $option_name );
		$value ='';

		if(isset($_POST["edit_post"])){
			$value = $input[$_POST["edit_post"]][$name];
		}

		echo '<div class="regular-text '. $args['class'] .'" id="' . $name . '" name="' . $name . '">'.htmlspecialchars($value).'</div>';
	}

	public function textArea( $args )
	{
		$name 			= $args['label_for'];
		$option_name	= $args['option_name'];
		$input 			= get_option( $option_name );
		$value 			= $args['value'];

		if(isset($_POST["edit_post"])){
			$post = get_option($_POST["edit_post"]);
			$post = get_option($_POST["edit_post"]);
			$value = $input[$_POST["edit_post"]][$name];
		}
		echo '<textarea class="regular-text '. $args['class'] .'" id="' . $name . '" name="'. $name . '" placeholder="' . $args['placeholder'] .'" rows="10">'.htmlspecialchars($value).'</textarea>';
	}

	public function checkBox( $args )
	{

		$name = $args['label_for'];
		$option_name = $args['option_name'];
		$input = get_option( $option_name );
		$value = $args['checked'];

		if(isset($_POST["edit_post"])){
			$value = $input[$_POST["edit_post"]][$name];
		}

		echo '<input type="checkbox" class="regular-text '. $args['class'] .'" id="' . $name . '" name="' . $name . '"' . $value . '>';
	}

	public function shortCode( $args )
	{
		echo '<a class="button shotcode" href="#shortcode-popup" data-value=\'[text id="" class="" placeholder="" name="" minlength="" maxlength="" required="true" ]\'>
				Text
			  </a>';

		echo '<a class="button shotcode" href="#shortcode-popup" data-value=\'[number class="" id="" name="" placeholder="enter number" minlength="" maxlength="" required="true" ]\'>
				Number
			  </a>';

		echo '<a class="button shotcode" href="#shortcode-popup" data-value=\'[email class="" id="" name="" placeholder="enter email" minlength="" maxlength="" required="true" ]\'>
				Email
			  </a>';

		echo '<a class="button shotcode" href="#shortcode-popup" data-value=\'[url class="" id="" name="" placeholder="" minlength="" maxlength="" required="true" ]\'>
				Url
			  </a>';

		echo '<a class="button shotcode" href="#shortcode-popup" data-value=\'[phone class="" id="" name="" placeholder="" minlength="" maxlength="" required="true" ]\'>
				Phone Number
			  </a>';

		echo '<a class="button shotcode" href="#shortcode-popup" data-value=\'[textarea class="" id="" name="" placeholder="" minlength="" maxlength="" required="true" ]\'>
				Text area
			  </a>';

		echo '<a class="button shotcode" href="#shortcode-popup-hidden" data-value=\'[hidden class="" id="" name="" ]\'>
				Hidden
			  </a>';

		echo '<a class="button shotcode file" href="#shortcode-popup-file" data-value=\'[file class="" id="" placeholder="" name="" filesize=""  filetype="" required="true" ]\'>
				File
			  </a>';

		echo '<a class="button shotcode" href="#shortcode-popup-submit" data-value=\'[submit class="" id="" value="submit"]\'>
				Submit Button
			  </a>';


		echo '<div id="shortcode-popup" class="overlay">
					<div class="popup">
						<div class="relative">
							<h3 class="popup-heading">Genrate html tag for <span></span> field</h3>
							<a class="close" href="#">&times;</a>
						</div>
						<div class="popup-content">
							<div class="d-flex align-center justify-between wrapper">
								<label>ID Attribute</label>
								<input type="text" id="id-attr" data-value="id" class="popup-fields" placeholder="enter ID"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Class Attribute</label>
								<input type="text" id="class-attr" data-value="class" class="popup-fields" placeholder="enter class"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Placeholder</label>
								<input type="text" id="placeholder-attr" data-value="placeholder" class="popup-fields" placeholder="enter placeholder"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Name Attribute</label>
								<input type="text" id="name-attr" data-value="name" class="popup-fields" placeholder="enter name"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Is this field required</label>
								<input type="checkbox" id="required-attr" data-value="required" class="popup-required" checked/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Min length</label>
								<input type="number" min="1" data-value="minlength" id="minlength-attr" class="popup-fields" />
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Max length</label>
								<input type="number" min="1" id="maxlength-attr" data-value="maxlength" class="popup-fields" />
							</div>

							<br>
							<div class="align-center justify-between wrapper">
								<input type="text" class="popup-tag-output" value=\'\' readonly>
								<a id="popup-submit" class="popup-submit wrapper-2 button button-primary" href="#"/>Insert Tag </a>
							</div>
						</div>
					</div>
			  </div>
			 ';

		echo '<div id="shortcode-popup-file" class="overlay">
					<div class="popup">
						<div class="relative">
							<h3 class="popup-heading">Genrate html tag for <span></span> field</h3>
							<a class="close" href="#">&times;</a>
						</div>
						<div class="popup-content">
							<div class="d-flex align-center justify-between wrapper">
								<label>ID Attribute</label>
								<input type="text" id="id-attr" data-value="id" class="popup-fields" placeholder="enter id"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Class Attribute</label>
								<input type="text" id="class-attr" data-value="class" class="popup-fields" placeholder="enter class"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Placeholder</label>
								<input type="text" id="placeholder-attr" data-value="placeholder" class="popup-fields" placeholder="enter placeholder"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Name Attribute</label>
								<input type="text" id="name-attr" data-value="name" class="popup-fields" placeholder="enter name"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Is this field required</label>
								<input type="checkbox" id="required-attr" data-value="required" class="popup-required" checked/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Max File size (in Kb)</label>
								<input type="number" min="1000" data-value="filesize" id="filesize-attr" class="popup-fields" placeholder="eg: 10000" />
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Allowed File Type
								</label>
								<input type="text" min="1" id="filetype-attr" data-value="filetype" class="popup-fields" placeholder="eg: pdf|doc|docx" />
							</div>

							<br>
							<div class="align-center justify-between wrapper">
								<input type="text" class="popup-tag-output" value=\'\' readonly>
								<a id="popup-submit" class="popup-submit wrapper-2 button button-primary" href="#"/>Insert Tag </a>
							</div>
						</div>
					</div>
			  </div>
			 ';

		echo '<div id="shortcode-popup-submit" class="overlay">
					<div class="popup">
						<div class="relative">
							<h3 class="popup-heading">Genrate html tag for <span></span> field</h3>
							<a class="close" href="#">&times;</a>
						</div>
						<div class="popup-content">
							<div class="d-flex align-center justify-between wrapper">
								<label>ID Attribute</label>
								<input type="text" id="id-attr" data-value="id" class="popup-fields" placeholder="enter id"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Class Attribute</label>
								<input type="text" id="class-attr" data-value="class" class="popup-fields" placeholder="enter class"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Value</label>
								<input type="text" id="value-attr" data-value="value" class="popup-fields" placeholder="eg: Submit form"/>
							</div>
							<br>
							<div class="align-center justify-between wrapper">
								<input type="text" class="popup-tag-output" value=\'\' readonly>
								<a id="popup-submit" class="popup-submit wrapper-2 button button-primary" href="#"/>Insert Tag </a>
							</div>
						</div>
					</div>
			  </div>
			 ';

		echo '<div id="shortcode-popup-hidden" class="overlay">
					<div class="popup">
						<div class="relative">
							<h3 class="popup-heading">Genrate html tag for <span></span> field</h3>
							<a class="close" href="#">&times;</a>
						</div>
						<div class="popup-content">
							<div class="d-flex align-center justify-between wrapper">
								<label>ID Attribute</label>
								<input type="text" id="id-attr" data-value="id" class="popup-fields" placeholder="enter id"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Class Attribute</label>
								<input type="text" id="class-attr" data-value="class" class="popup-fields" placeholder="enter class"/>
							</div>

							<div class="d-flex align-center justify-between wrapper">
								<label>Name Attribute</label>
								<input type="text" id="name-attr" data-value="name" class="popup-fields" placeholder="enter name"/>
							</div>
							<br>
							<div class="align-center justify-between wrapper">
								<input type="text" class="popup-tag-output" value=\'\' readonly>
								<a id="popup-submit" class="popup-submit wrapper-2 button button-primary" href="#"/>Insert Tag </a>
							</div>
						</div>
					</div>
			  </div>
			 ';
	}
}