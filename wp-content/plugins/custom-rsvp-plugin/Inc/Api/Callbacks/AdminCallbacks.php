<?php

/**
 * 
*/
namespace IncRsvp\Api\Callbacks;

use IncRsvp\Base\BaseController;


/**
 * 
*/

class AdminCallbacks extends BaseController
{
	public function adminDashboard()
	{
		return require_once( "$this->plugin_path/templates/signupforms.php");
	}

	public function addNewForms()
	{
		return require_once( "$this->plugin_path/templates/addnewform.php");
	}

	public function settings()
	{
		return require_once( "$this->plugin_path/templates/admin.php");
	}
}