<?php 

namespace IncRsvp;


final class Init
{
	/**
		* Store all class inside the array
		* @return array full list of classes
	*/
	public static function get_services()
	{
		return[
			Pages\Admin::class,
			Base\Enqueue::class,
			Base\SettingsLink::class,
			Base\ShortcodeController::class,
			Base\PostController::class,
			Base\CustomSettingsController::class,
			Base\FormSubmitController::class,
		];
	}
	
	/**
		* loops through the classes. initalize them, 
		* and call register() mehtod id it exists
		* @return
	*/
	public static function register_services()
	{
		foreach ( self::get_services() as $class) {
			$service = self::instantiate( $class );
			if( method_exists($service, 'register')){
				$service->register();
			}
		}
	}

	/**
		* Initialize the class
		* @param class $class class from the services array
		* @return class instance new instance of the class
	*/

	private static function instantiate( $class ){
		$service = new $class(); 
		return $service;
	}
}
