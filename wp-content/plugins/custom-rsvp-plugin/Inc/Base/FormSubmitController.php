<?php

/**
 * @package RSVP
*/

namespace IncRsvp\Base;

use IncRsvp\Api\SettingsApi;
use IncRsvp\Base\BaseController;
use IncRsvp\Api\Callbacks\PostCallbacks;
use IncRsvp\Api\Callbacks\AdminCallbacks;

class FormSubmitController extends BaseController
{ 
	function register()
	{
        add_action('wp_ajax_rsvp_submit',array( $this, 'rsvp_submit'));
        add_action('wp_ajax_nopriv_rsvp_submit',array( $this, 'rsvp_submit'));
    	add_action('wp_loaded', array( $this, 'register_rsvp_signup_assets'));    	
	}

	public function rsvp_submit($atts)
	{	

		session_start();
		global $wpdb;

		$options 		 		= get_option('rsvp_plugin_post') ?: [];
		$post_id 	 			= sanitize_text_field($_POST['post_id']);
		$enabled_captcha 		= $options[$post_id]["enable_captcha"];
        $response->status 		= false;
        $response->invalid 		= true;
        $response->message 		= '';
        $response->data 		= 0;

		if($enabled_captcha == "checked"){
			$captcha 		 		= $_POST['g-recaptcha-response'];
			$secretKey		 		= $options['Site-Details']['secret_key'];
			$recaptcha_url 			= 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
	        
	        $recaptcha_response 	= file_get_contents($recaptcha_url);
	        $recaptcha_responseKeys = json_decode($recaptcha_response,true);
        	$response->data 		= 1;

			if(!$captcha == "checked"){
				$response->message 	=  'Invalid captcha';
				echo json_encode($response);
				wp_die();
			}
		}

		if($enabled_captcha == "checked" && !$recaptcha_responseKeys["success"]) {
            $response->message = 'Captcha Failed, Please try again';   
			echo json_encode($response);
			wp_die();
        }

		if(empty($options[$post_id])){
			$response->message = "There was an error trying to send your message.";
			echo json_encode($response);
			wp_die();
		}

		$form_content = $options[$post_id]['content'];
		preg_match_all('/(?<=\[(?!file)).*?name="(.*?)"(.*?)(?=\])/', $form_content, $fieldNames);

		if (count($fieldNames[1]) != count($_POST['rsvp'])) {
			$response->message = "There was an error trying to send your message.";
			echo json_encode($response);
		}else {
				$form_data				= [];
				$blackList_websites		= $options['Site-Details']['blacklist'];
				$blackList_subdomains	= $options['Site-Details']['blacklist-subdomain'];
				$blackList_emails		= $options['Site-Details']['blacklist-email'];
				$content_type_text 		= $this->sanitizeValue($form_content,'text' , $response->data);
				$content_type_number 	= $this->sanitizeValue($form_content,'number' , $response->data);
				$content_type_hidden 	= $this->sanitizeValue($form_content,'hidden' , $response->data);
				$content_type_email		= $this->sanitizeValue($form_content,'email' , $response->data);
				$content_type_url		= $this->sanitizeValue($form_content,'url' , $response->data);
				$content_type_phone		= $this->sanitizeValue($form_content,'phone' , $response->data);
				$content_type_textarea	= $this->sanitizeValue($form_content,'textarea' , $response->data);
								
				foreach ($content_type_text as $key => $value) {
					$form_data[$key] = $value;
				}
								
				foreach ($content_type_number as $key => $value) {
					$form_data[$key] = $value;
				}

				foreach ($content_type_hidden as $key => $value) {
					$form_data[$key] = $value;
				}

				foreach ($content_type_textarea as $key => $value) {
					$form_data[$key] = $value;
				}

				foreach ($content_type_email as $key => $value) {
					$form_data[$key] = $value;
					foreach ($blackList_emails as $email) {
						$domain = explode('@',$email);
						if(strpos($form_data[$key], $domain[0])){
							$response->message =  'Please enter a valid Email';
							echo json_encode($response);
							wp_die();
						}
					}
				}

				foreach ($content_type_url as $key => $value) {
					$form_data[$key] = $value;
					$parse_url 		 = $this->parse_url_all($value);
					foreach ($blackList_websites as $web_url) {
						$domain = $this->parse_url_all($web_url);
						if($parse_url['domainX'] == $domain['domainX']){
							$response->message =  'Please enter a valid Website';
							echo json_encode($response);
							wp_die();
						}
					}
					foreach ($blackList_subdomains as $web_url) {
						if(strpos($value,$web_url) !== false){
							$response->message =  'Please enter a valid subDomain';
							echo json_encode($response);
							wp_die();
						}		
					}
				}

				foreach ($content_type_phone as $key => $value) {
					$form_data[$key] = $value;
				}
				$args = array(
					'post_id' 		=> $post_id,
				);
				$data = array(
		            'form_id' 		=> $post_id,
		            'user_IP' 		=> $_SERVER["REMOTE_ADDR"],
		            'signup_date' 	=> date('Y-m-d h:m:s'),
		            'form_data'		=> serialize($form_data)
				);

				$to      			= $this->convert_shortcode($options[$post_id]['mail_to'],$form_data);
				$subject 			= $this->convert_shortcode($options[$post_id]['mail_subject'] ,$form_data);
				$From 	 			= $this->convert_shortcode($options[$post_id]['mail_from'] ,$form_data);
				$headers 			= 'From:'.$From."\r\n";
				$headers 			.= "MIME-Version: 1.0\r\n";
				$headers 			.= "Content-Type: text/html; charset=utf-8\n\n";
				$message 			= stripslashes($this->convert_shortcode($options[$post_id]['mail_body'],$form_data));
				$attachment 		= $this->validateFile( $form_content );
				$mail2 				= $options[$post_id]["mail2"];
				$mailSent			= false;
				$mailSent2			= true;
				if($mail2 == 'checked'){
					$to2      			= $this->convert_shortcode($options[$post_id]['mail_to_2'],$form_data);
					$subject2 			= $this->convert_shortcode($options[$post_id]['mail_subject_2'] ,$form_data);
					$From2 	 			= $this->convert_shortcode($options[$post_id]['mail_from_2'] ,$form_data);
					$headers2 			= 'From:'.$From."\r\n";
					$headers2 			.= "MIME-Version: 1.0\r\n";
					$headers2 			.= "Content-Type: text/html; charset=utf-8\n\n";
					$message2 			= stripslashes($this->convert_shortcode($options[$post_id]['mail_body_2'],$form_data));
					$attachment2 		= $this->validateFile( $form_content );
					$mailSent2		 	= wp_mail( $to2, $subject2, $message2, $headers2, $attachment2);
				}

				$mailSent = wp_mail( $to, $subject, $message, $headers, $attachment);

				if ( $mailSent && $mailSent2 ) {
					unlink($attachment);
					unlink($attachment2);
					if($options[$post_id]['save_in_db'] != ''){
						$wpdb->insert('wp_rsvp_signups', $data );
					}
					$response->status 	= true;
		        	$response->message 	= "Request completed successfully!";
		        	$response->invalid 	= false;
					echo json_encode($response);
		        }else{
					$response->message = "There was an error trying to send your message. Please try again later.";
					echo json_encode($response);
		        }
			}
        wp_die();
	}

	public function sanitizeValue($data,$type,$captcha)
	{
		$response->invalid 	= false;
		$response->status 	= false;
		$response->data 	= $captcha;
		$options 			= get_option('rsvp_plugin_post') ?: [];
		$blackList_inputs	= $options['Site-Details']['blacklist-input'];

		$reg 		= '/(?<=\[)'.$type.'.*?name="(.*?)"(.*?)(?=\])/';
		$required 	= '/(?<=\[)'.$type.'.*?required="(.*?)"(.*?)(?=\])/';

		preg_match_all($reg, $data, $matches);
		preg_match_all($required, $data, $matchReq);

		foreach ($matches[1] as $key => $inputvalue) {
			$value =  $_POST['rsvp'][$inputvalue];
	        if(stripos($value, 'http://')!== false || stripos($value, 'https://')!== false ){
	          $split_value = explode('://', $value);
	          $value = $split_value[1];
	        }
	        foreach ($blackList_inputs as $banned) {
	            if(strpos($value, $banned) !== false){
	                $response->message = "Please enter a valid ".$inputvalue;
	                $response->invalid   = true;
					echo json_encode($response);
					wp_die();
	            }
	        }
		}

		($type == 'textarea') ? $type = 0 : '';

		switch ($type) {
			case 0:
				foreach ($matches[1] as $key => $value) {
					$form_data[$value] = sanitize_textarea_field($_POST['rsvp'][$value]);
					if($matchReq[1][$key] == 'true' && $form_data[$value] == ""){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}
				}
				return $form_data;
				break;

			case 'text':
				foreach ($matches[1] as $key => $value) {
					$form_data[$value] = sanitize_text_field($_POST['rsvp'][$value]);
					$textReg = "/^[A-Za-z0-9 \\\\']+$/";
					if($form_data[$value]!= "" && preg_match($textReg,$form_data[$value]) == '0'){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}elseif($matchReq[1][$key] == 'true' && $form_data[$value] == ""){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}
				}
				return $form_data;
				break;

			case 'email':
				foreach ($matches[1] as $key => $value) {
					$form_data[$value] = sanitize_email($_POST['rsvp'][$value]);
					if(($matchReq[1][$key] == 'true' && $form_data[$value] == "")){
						$response->message = "Please enter a valid ".$value;
						echo json_encode($response);
						wp_die();
					}
				}
				return $form_data;
				break;

			case 'url':
				foreach ($matches[1] as $key => $value) {
					$webReg = '/^(http:\/\/|https:\/\/)?[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/';
					$form_data[$value] = sanitize_url($_POST['rsvp'][$value]);
					if($form_data[$value]!= "" && preg_match($webReg, $form_data[$value]) == '0'){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}elseif(($matchReq[1][$key] == 'true' && $form_data[$value] == "")){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}
				}
				return $form_data;
				break;

			case 'phone':
				foreach ($matches[1] as $key => $value) {
					$form_data[$value] = filter_var($_POST['rsvp'][$value], FILTER_SANITIZE_NUMBER_INT);
					if($matchReq[1][$key] == 'true' && $form_data[$value] == ""){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}
				}
				return $form_data;
				break;

			case 'number':
				foreach ($matches[1] as $key => $value) {
					$form_data[$value] = filter_var($_POST['rsvp'][$value], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
					if($matchReq[1][$key] == 'true' && $form_data[$value] == ""){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}
				}
				return $form_data;
				break;

			default: foreach ($matches[1] as $key => $value) {
					$form_data[$value] = wp_strip_all_tags($_POST['rsvp'][$value]);
					if($matchReq[$key] == 'true' && $form_data[$value] == ""){
						$response->message = "Please enter a valid ".$value;
						$response->invalid   = true;
						echo json_encode($response);
						wp_die();
					}
				}
				return $form_data;
				break;
		}
	}

	public function validateFile( $data )
	{
		$response->status = false;
		$fileSize 		= '/(?<=\[)file.*?filesize="(.*?)"(.*?)(?=\])/';
		$fileType 		= '/(?<=\[)file.*?filetype="(.*?)"(.*?)(?=\])/';
		$required 		= '/(?<=\[)file.*?required="(.*?)"(.*?)(?=\])/';
		$uploadedFile 	= $_FILES['file']["name"];
		preg_match_all($fileSize, $data, $matchSize);
		preg_match_all($fileType, $data, $matchType);
		preg_match_all($required, $data, $matchReq);
		if ($matchReq[1][0] == 'true' && $uploadedFile == "") {
			$response->message = "Please Upload File";
			echo json_encode($response);
			wp_die();
		}else if($uploadedFile != ""){
			$allowed_size		= $matchSize[1][0];
			$allowed_type 		= explode("|",$matchType[1][0]);
			$allowed_value 		= implode(', .', $allowed_type);
			$mime_type = [
				'aac'	 		=> 'audio/aac',
				'abw'	 		=> 'application/x-abiword',
				'arc'	 		=> 'application/x-freearc',
				'avi'	 		=> 'video/x-msvideo',
				'azw'	 		=> 'application/vnd.amazon.ebook',
				'bin'	 		=> 'application/octet-stream',
				'bmp'	 		=> 'image/bmp',
				'bz'	 		=> 'application/x-bzip',
				'bz2'	 		=> 'application/x-bzip2',
				'csh'	 		=> 'application/x-csh',
				'css'	 		=> 'text/css',
				'csv'	 		=> 'text/csv',
				'doc'	 		=> 'application/msword',
				'docx'	 		=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'eot'	 		=> 'application/vnd.ms-fontobject',
				'epub'	 		=> 'application/epub+zip',
				'gz'	 		=> 'application/gzip',
				'gif'	 		=> 'image/gif',
				'htm'	 		=> 'text/html',
				'html'	 		=> 'text/html',
				'ico'	 		=> 'image/vnd.microsoft.icon',
				'ics'	 		=> 'text/calendar',
				'jar'	 		=> 'application/java-archive',
				'jpeg'	 		=> 'image/jpeg',
				'jpg'	 		=> 'image/jpeg',
				'js'	 		=> 'text/javascript',
				'json'	 		=> 'application/json',
				'jsonld'	 	=> 'application/ld+json',
				'mjs'	 		=> 'text/javascript',
				'mp3'	 		=> 'audio/mpeg',
				'mpeg'	 		=> 'video/mpeg',
				'mpkg'	 		=> 'application/vnd.apple.installer+xml',
				'odp'	 		=> 'application/vnd.oasis.opendocument.presentation',
				'ods'	 		=> 'application/vnd.oasis.opendocument.spreadsheet',
				'odt'	 		=> 'application/vnd.oasis.opendocument.text',
				'oga'	 		=> 'audio/ogg',
				'ogv'	 		=> 'video/ogg',
				'ogx'	 		=> 'application/ogg',
				'opus'	 		=> 'audio/opus',
				'otf'	 		=> 'font/otf',
				'png'	 		=> 'image/png',
				'pdf'	 		=> 'application/pdf',
				'php'	 		=> 'application/x-httpd-php',
				'ppt'	 		=> 'application/vnd.ms-powerpoint',
				'pptx'	 		=> 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
				'rar'	 		=> 'application/vnd.rar',
				'rtf'	 		=> 'application/rtf',
				'sh'	 		=> 'application/x-sh',
				'svg'	 		=> 'image/svg+xml',
				'swf'	 		=> 'application/x-shockwave-flash',
				'tar'	 		=> 'application/x-tar',
				'tif'	 		=> 'image/tiff',
				'tiff'	 		=> 'image/tiff',
				'ts'	 		=> 'video/mp2t',
				'ttf'	 		=> 'font/ttf',
				'txt'	 		=> 'text/plain',
				'vsd'	 		=> 'application/vnd.visio',
				'wav'	 		=> 'audio/wav',
				'weba'	 		=> 'audio/webm',
				'webm'	 		=> 'video/webm',
				'webp'	 		=> 'image/webp',
				'woff'	 		=> 'font/woff',
				'woff2'	 		=> 'font/woff2',
				'xhtml'	 		=> 'application/xhtml+xml',
				'xls'	 		=> 'application/vnd.ms-excel',
				'xlsx'	 		=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				'xml'	 		=> 'text/xml',
				'xml'	 		=> 'application/xml',
				'xul'	 		=> 'application/vnd.mozilla.xul+xml',
				'zip'	 		=> 'application/zip',
				'3gp'	 		=> 'audio/3gpp',
				'3gp'	 		=> 'video/3gpp',
				'3g2'	 		=> 'audio/3gpp2',
				'3g2'	 		=> 'video/3gpp2',
				'7z'	 		=> 'application/x-7z-compressed',
			];
			foreach ($allowed_type as $key => $value) {
				$allowed_type[$key] = $mime_type[$value];
			};
			$allowed_type_value = implode(', .', $allowed_type);
			$temp		  		= $_FILES['file']['tmp_name'];
			$ext 		  		= mime_content_type($temp);
		
			if(!in_array($ext, $allowed_type)){
				// echo "string".implode(', .', $allowed_type);
	    		$response->message = "Invalid File! Supported formats are ".$allowed_value;
				echo json_encode($response);
				wp_die();
			}

			if($_FILES['file']['size'] >= $allowed_size){	    		
	    		$response->message = "Error Uploading File!";
				echo json_encode($response);
				wp_die();
			}
			
			move_uploaded_file( $temp, WP_CONTENT_DIR.'/uploads/'.basename( $_FILES['file']['name'] ));
			$attachment = WP_CONTENT_DIR.'/uploads/'.basename( $_FILES['file']['name'] );
		}

		return $attachment;
	}

	public function parse_url_all($url){
	    $url = substr($url,0,4) == 'http' ? $url: 'http://'.$url;
	    $d = parse_url($url);
	    $tmp = explode('.',$d['host']);
	    $n = count($tmp);
	    if ($n>=2){
	        if ($n==4 || ($n==3 && strlen($tmp[($n-2)])<=3)){
	            $d['domain'] = $tmp[($n-4)].".".$tmp[($n-3)].".".$tmp[($n-2)];
	            $d['domainX'] = $tmp[($n-3)];
	            $d['subDomain'] = $tmp[($n-2)];
	            $d['ext'] = $tmp[($n-1)];
	        }elseif(!($tmp[0] == 'www') && ($n==3 && strlen($tmp[($n-2)])<=3)) {
	            $d['domain'] = $tmp[($n-3)].".".$tmp[($n-2)];
	            $d['domainX'] = $tmp[($n-3)];
	            $d['subDomain'] = $tmp[($n-2)];
	            $d['ext'] = $tmp[($n-1)];
	        }else{	        	
	            $d['domain'] = $tmp[($n-2)].".".$tmp[($n-1)];
	            $d['domainX'] = $tmp[($n-2)];
	            $d['ext'] = $tmp[($n-1)];
	        }
	    }
	    return $d;
	}

	public function convert_shortcode($data, $form_data){
		preg_match_all('/(?<=\[)(.*?)(?=\])/', $data, $matches);
			foreach($matches[0] as $key => $value)
			{
			  $matches[0][$key] = str_replace('\\', '', $form_data[$value]);
			}
			$count = 0;
			$terms = $matches[0];
			$data =  preg_replace_callback('/\[.*?\]/',function($match) use (&$count, $terms) {
			    $return = !empty($terms[$count]) ? $terms[$count]." " : '';
			    $count++;
			    return $return;
			}, $data);

			return $data;
	}

    public function register_rsvp_signup_assets()
    {
    	$options = get_option('rsvp_plugin_post') ?: [];
        wp_register_style('rsvp-css', $this->plugin_url.'assets/form.css',false);
    	wp_deregister_script('jquery');
		wp_register_script('jquery', $this->plugin_url.'assets/jquery.min.js', false, null, true);
		wp_register_script('jquery-validator',$this->plugin_url.'assets/jquery.validate.min.js', array('jquery'), null, true);
		wp_register_script('validator-additional',$this->plugin_url.'assets/additional-methods.min.js', array('jquery-validator'), null, true);
		wp_register_script('form-scripts',$this->plugin_url.'assets/form.js', false, null, true);
    }
}