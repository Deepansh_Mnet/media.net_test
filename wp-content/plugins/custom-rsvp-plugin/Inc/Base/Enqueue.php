<?php

/**
 * 
*/
namespace IncRsvp\Base;

use IncRsvp\Base\BaseController;

/**
 * 
*/
class Enqueue extends BaseController
{ 
	function register(){
		add_action('admin_menu',array($this,'enqueue'));
	}

	function enqueue(){
		wp_enqueue_style('pluginstyle' , $this->plugin_url.'/assets/style.css');
		wp_enqueue_script('jquery-plugin' , $this->plugin_url.'/assets/jquery.min.js');
		wp_enqueue_script('pluginscript' , $this->plugin_url.'/assets/custom.js');
	}

}