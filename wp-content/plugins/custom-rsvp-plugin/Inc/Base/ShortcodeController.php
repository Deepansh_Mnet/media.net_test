<?php

/**
 * @package RSVP
*/

namespace IncRsvp\Base;

use IncRsvp\Api\SettingsApi;
use IncRsvp\Base\BaseController;
use IncRsvp\Api\Callbacks\PostCallbacks;
use IncRsvp\Api\Callbacks\AdminCallbacks;

class ShortcodeController extends BaseController
{ 
	public $captchaValue = 'no';
	function register()
	{
		$this->shortCode();
	}

	public function shortCode() 
	{
        add_shortcode('signupform', array($this, 'initialiseSignUpForm'));
        add_shortcode( 'text' , array($this, 'input_text_shortcode' ));
        add_shortcode( 'email' , array($this, 'input_email_shortcode' ));
        add_shortcode( 'number' , array($this, 'input_number_shortcode' ));
        add_shortcode( 'url' , array($this, 'input_url_shortcode' ));
        add_shortcode( 'phone' , array($this, 'input_phone_shortcode' ));
        add_shortcode( 'file' , array($this, 'input_file_shortcode' ));
        add_shortcode( 'textarea' , array($this, 'input_textarea_shortcode' ));
        add_shortcode( 'hidden' , array($this, 'input_hidden_shortcode' ));
        add_shortcode( 'submit' , array($this, 'input_submit_shortcode', ));
	}
	public function input_textarea_shortcode( $attr )
	{	 
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'name',
	            'placeholder' 	=> 'placeholder',
	            'required'		=> '',
	            'maxlength'		=> '',
	            'minlength'		=> '',
	        ), $attr );

	    $minlength = ($args['minlength'] == "") ? '' :'data-rule-minlength="'.$args['minlength'].'"';
	    $maxlength = ($args['maxlength'] == "") ? '' :'data-rule-maxlength="'.$args['maxlength'].'"';
	    
	    $output = '<div class="form-input-wrap">
	    				<textarea id="'.$args['id'].'" 
					 		name="rsvp['.$args['name'].']" 
					 		class="'.$args['class'].'" 
					 		placeholder="'.$args['placeholder'].'"
					 		data-rule-required="'.$args['required'].'"
					 		'.$minlength.'
					 		'.$maxlength.'
					 	></textarea>
		 		   </div>';

	    return $output;
	}

	public function input_text_shortcode( $attr )
	{	 
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'name',
	            'placeholder' 	=> 'placeholder',
	            'required'		=> '',
	            'maxlength'		=> '',
	            'minlength'		=> '',
	        ), $attr );

	    $minlength = ($args['minlength'] == "") ? '' :'data-rule-minlength="'.$args['minlength'].'"';
	    $maxlength = ($args['maxlength'] == "") ? '' :'data-rule-maxlength="'.$args['maxlength'].'"';

	    $output = '<div class="form-input-wrap"><input 
				 		type="text" 
				 		id="'.$args['id'].'" 
				 		name="rsvp['.$args['name'].']" 
				 		class="'.$args['class'].'" 
				 		placeholder="'.$args['placeholder'].'"
				 		'.$minlength.'
				 		'.$maxlength.'
				 		data-rule-required="'.$args['required'].'"
		 		   /></div>';

	    return $output;
	}

	public function input_email_shortcode( $attr )
	{	 
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'email',
	            'placeholder' 	=> 'placeholder',
	            'required'		=> '',
	            'maxlength'		=> '',
	            'minlength'		=> '',

	        ), $attr );

	    $minlength = ($args['minlength'] == "") ? '' :'data-rule-minlength="'.$args['minlength'].'"';
	    $maxlength = ($args['maxlength'] == "") ? '' :'data-rule-maxlength="'.$args['maxlength'].'"';

	    $output = '<div class="form-input-wrap"><input 
				 		type="email" 
				 		id="'.$args['id'].'"
				 		name="rsvp['.$args['name'].']" 
				 		class="'.$args['class'].'" 
				 		placeholder="'.$args['placeholder'].'"
				 		'.$minlength.'
				 		'.$maxlength.'
				 		data-rule-required="'.$args['required'].'"
		 		   /></div>';

	    return $output;
	}

	public function input_url_shortcode( $attr )
	{	 
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'website',
	            'placeholder' 	=> 'placeholder',
	            'required'		=> '',
	            'maxlength'		=> '',
	            'minlength'		=> '',

	        ), $attr );

	    $minlength = ($args['minlength'] == "") ? '' :'data-rule-minlength="'.$args['minlength'].'"';
	    $maxlength = ($args['maxlength'] == "") ? '' :'data-rule-maxlength="'.$args['maxlength'].'"';


	    $output = '<div class="form-input-wrap"><input 
				 		type="url" 
				 		id="'.$args['id'].'" 
				 		name="rsvp['.$args['name'].']" 
				 		class="'.$args['class'].'" 
				 		placeholder="'.$args['placeholder'].'"
				 		'.$minlength.'
				 		'.$maxlength.'
				 		data-rule-required="'.$args['required'].'"
		 		  	/></div>';

	    return $output;
	}

	public function input_phone_shortcode( $attr )
	{	 
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'phone',
	            'placeholder' 	=> 'placeholder',
	            'required'		=> '',
	            'maxlength'		=> '',
	            'minlength'		=> '',	            
	        ), $attr );

	    $minlength = ($args['minlength'] == "") ? '' :'data-rule-minlength="'.$args['minlength'].'"';
	    $maxlength = ($args['maxlength'] == "") ? '' :'data-rule-maxlength="'.$args['maxlength'].'"';


	    $output = '<div class="form-input-wrap"><input 
				 		type 				="tel" 
				 		id 					="'.$args['id'].'" 
				 		name 				="rsvp['.$args['name'].']" 
				 		class 				="'.$args['class'].'" 
				 		placeholder 		="'.$args['placeholder'].'"
				 		'.$minlength.'
				 		'.$maxlength.'
				 		data-rule-required	="'.$args['required'].'"
				 		data-rule-phone		="true"
		 		   /></div>';

	    return $output;
	}

	public function input_number_shortcode( $attr )
	{	 
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'number',
	            'placeholder' 	=> 'placeholder',
	            'required'		=> '',
	            'maxlength'		=> '',
	            'minlength'		=> '',	            
	        ), $attr );

	    $minlength = ($args['minlength'] == "") ? '' :'data-rule-minlength="'.$args['minlength'].'"';
	    $maxlength = ($args['maxlength'] == "") ? '' :'data-rule-maxlength="'.$args['maxlength'].'"';


	    $output = '<div class="form-input-wrap"><input 
				 		type 				="number" 
				 		id 					="'.$args['id'].'" 
				 		name 				="rsvp['.$args['name'].']" 
				 		class 				="'.$args['class'].'" 
				 		placeholder 		="'.$args['placeholder'].'"
				 		'.$minlength.'
				 		'.$maxlength.'
				 		data-rule-required	="'.$args['required'].'"
		 		   /></div>';

	    return $output;
	}

	public function input_file_shortcode( $attr )
	{	 
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'file',
	            'placeholder' 	=> 'placeholder',
	            'required'		=> '',
	            'filesize'		=> '',
	            'filetype'		=> '',

	        ), $attr );

	    $output = '<div class="form-input-wrap"><input 
				 		type="file" 
				 		id="'.$args['id'].'" 
				 		name="rsvp['.$args['name'].']" 
				 		class="'.$args['class'].'" 
				 		placeholder="'.$args['placeholder'].'"
				 		data-rule-filesize="'.$args['filesize'].'"
				 		data-rule-extension="'.$args['filetype'].'"
				 		data-rule-required="'.$args['required'].'"
		 		   /></div>';

	    return $output;
	}

	public function input_hidden_shortcode( $attr )
	{	
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'file',
	            'value' 		=> '',

	        ), $attr );

	    $output = '<div class="form-input-wrap"><input 
				 		type="hidden" 
				 		id="'.$args['id'].'" 
				 		name="rsvp['.$args['name'].']" 
				 		class="'.$args['class'].'" 
				 		value="'.$args['value'].'"
				 		required="false"
		 		   /></div>';

	    return $output;
	}

	public function input_submit_shortcode( $attr )
	{	 
		$options 	= get_option('rsvp_plugin_post') ?: [];
		$site_key 	= $options['Site-Details']['site_key'];
	    $args = shortcode_atts( array(
	            'class' 		=> '',
	            'id' 			=> '',
	            'name' 			=> 'rsvp_submit',
	            'value' 		=> 'submit'
	        ), $attr );

	    if($this->captchaValue == "checked"){
	    	$output = '<div class="g-recaptcha" data-sitekey="'.$site_key.'"></div>
	    				<input 
					 		type="submit" 
					 		id="'.$args['id'].'" 
					 		name="'.$args['name'].'" 
					 		class="'.$args['class'].'" 
					 		value="'.$args['value'].'"
			 		   />
			 		   <script src="https://www.google.com/recaptcha/api.js" async defer></script>
			 		   ';

	    }else{
		    $output = '<div class="form-input-wrap"><input 
					 		type="submit" 
					 		id="'.$args['id'].'" 
					 		name="'.$args['name'].'" 
					 		class="'.$args['class'].'" 
					 		value="'.$args['value'].'"
			 		   /></div>';

	    }
	    return $output;
	}

	private function enqueue_assets(){
		wp_enqueue_style('rsvp-css');
		wp_enqueue_script('jquery-validator');
		wp_enqueue_script('validator-additional');
		wp_enqueue_script('form-scripts');
		wp_enqueue_script('jquery');
	}

	public function initialiseSignUpForm($atts, $content = null)
    {
    	$this->enqueue_assets();
		$options = get_option('rsvp_plugin_post') ?: [];
    	$this->captchaValue = $options[$atts['id']]['enable_captcha'];
    	extract(shortcode_atts(array(), $atts));
		if (!empty($options)) {
			
			$content_post	= $options[$atts['id']]['content'];
			$posts = apply_filters('the_content', $content_post);

	        $return='
	        <form id="rsvp-form" class="rsvp-form" name="rsvp-form" action="" method="post" data-url="'.admin_url("admin-ajax.php").'" enctype="multipart/form-data">'
	        			.$posts.
	    			'<input type="hidden" name="post_id" id="1" value="'.$atts["id"].'"/>
	    			<input type="hidden" name="action" id="'.$atts["id"].'" value="rsvp_submit"/>
	    			<p class="server_resp"></p>
			</form>';				
        	
       	return $return;			

		}
    }	
}