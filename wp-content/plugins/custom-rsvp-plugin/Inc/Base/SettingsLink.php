<?php

/**
 * @package RSVP
*/

namespace IncRsvp\Base;

use IncRsvp\Base\BaseController;

class SettingsLink extends BaseController
{	
	public function register() 
	{
		add_filter( "plugin_action_links_$this->plugin", array( $this, 'settings_link' ) );
	}

	public function settings_link( $links ) 
	{
		$admin_url		= admin_url();
		$options 		= get_option('rsvp_plugin_post') ?: [];
		$settings_link	= '<a href="'.admin_url("admin.php?page=rsvp_plugin_settings").'">Settings</a>';
		$dashboard_link = '<a href="'.admin_url("admin.php?page=rsvp_plugin").'">Dashboard</a>';
		if(empty($options) || current_user_can('administrator')){
			array_push( $links, $settings_link);
		}

		array_push( $links, $dashboard_link );
		return $links;
	}
}

