<?php

/**
 * @package RSVP
*/

namespace IncRsvp\Base;

use IncRsvp\Api\SettingsApi;
use IncRsvp\Base\BaseController;
use IncRsvp\Api\Callbacks\CustomSettingsCallbacks;
use IncRsvp\Api\Callbacks\AdminCallbacks;

class CustomSettingsController extends BaseController
{ 
	public $settings;

	public $callbacks;

	public $custom_settings_callbacks;

	public $subpages = array();

	public $custom_post = [];

	public $imagedata;

	function register()
	{
		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();
		
		$this->custom_settings_callbacks = new CustomSettingsCallbacks();

		$this->setSubpages();
		
		$this->setSettings();

		$this->setSections();

		$this->setFields();

		$this->settings->addSubPages( $this->subpages )->register();

		if(isset($_POST['add_setting_data'])){
			add_action('init',array( $this, 'registerData') );
		}

		if(isset($_POST['add_blacklist_data'])){
			add_action('init',array( $this, 'blacklistDomin') );
		}

		if(isset($_POST['add_blacklist_subdomain'])){
			add_action('init',array( $this, 'blacklistSubdomain') );
		}

		if(isset($_POST['add_blacklist_email'])){
			add_action('init',array( $this, 'blacklistEmail') );
		}

		if(isset($_POST['add_blacklist_input'])){
			add_action('init',array( $this, 'blacklistInput') );
		}

	}

	public function setSubpages()
	{
		$this->subpages = array(
	 		array(
				'parent_slug' => 'custom-rsvp-plugin', 
				'page_title' => 'Custom Post Type', 
				'menu_title' => 'Post Manager', 
				'capability' => 'manage_options', 
				'menu_slug' => 'rsvp_captcha', 
				'callback' => array( $this->callbacks, 'adminPost' )
			),
	 		array(
				'parent_slug' => 'custom-rsvp-plugin', 
				'page_title' => 'Custom Post Type', 
				'menu_title' => 'Post Manager', 
				'capability' => 'manage_options', 
				'menu_slug' => 'rsvp_blacklist', 
				'callback' => array( $this->callbacks, 'adminPost' )
			),
		);
	}

	public function setSettings()
	{
		$args = array(
			array(
				'option_group' => 'rsvp_captcha_settings',
				'option_name' => 'rsvp_plugin_post',
				'callback' => array($this->custom_settings_callbacks, 'settingSanitize')
			),
			array(
				'option_group' => 'rsvp_blacklist_settings',
				'option_name' => 'rsvp_plugin_post',
				'callback' => array($this->custom_settings_callbacks, 'settingSanitize')
			),
			array(
				'option_group' => 'rsvp_blacklist_subdomin_settings',
				'option_name' => 'rsvp_plugin_post',
				'callback' => array($this->custom_settings_callbacks, 'settingSanitize')
			),
			array(
				'option_group' => 'rsvp_blacklist_email_settings',
				'option_name' => 'rsvp_plugin_post',
				'callback' => array($this->custom_settings_callbacks, 'settingSanitize')
			),
			array(
				'option_group' => 'rsvp_blacklist_input_settings',
				'option_name' => 'rsvp_plugin_post',
				'callback' => array($this->custom_settings_callbacks, 'settingSanitize')
			),
			
		);
		$this->settings->setSettings( $args );
	}

	public function setSections()
	{
		$args = array(
			array(
				'id' 		=> 'rsvp_captcha',
				'title' 	=> '',
				'callback'	=> array($this->custom_settings_callbacks, 'settingsSectionManager'),
				'page' 		=> 'rsvp_captcha'
			),
			array(
				'id' 		=> 'rsvp_blacklist',
				'title' 	=> '',
				'callback'	=> array($this->custom_settings_callbacks, 'settingsSectionManager'),
				'page' 		=> 'rsvp_blacklist'
			),
			array(
				'id' 		=> 'rsvp_blacklist_subdomain',
				'title' 	=> '',
				'callback'	=> array($this->custom_settings_callbacks, 'settingsSectionManager'),
				'page' 		=> 'rsvp_blacklist_subdomain'
			),
			array(
				'id' 		=> 'rsvp_blacklist_email',
				'title' 	=> '',
				'callback'	=> array($this->custom_settings_callbacks, 'settingsSectionManager'),
				'page' 		=> 'rsvp_blacklist_email'
			),
			array(
				'id' 		=> 'rsvp_blacklist_input',
				'title' 	=> '',
				'callback'	=> array($this->custom_settings_callbacks, 'settingsSectionManager'),
				'page' 		=> 'rsvp_blacklist_input'
			),
		);

		$this->settings->setSections( $args );
	}

	public function setFields()
	{
		$args = array(
			array(
				'id' => 'site_key',
				'title' => 'Site Key',
				'callback' => array($this->custom_settings_callbacks, 'textField'),
				'page' => 'rsvp_captcha',
				'section' => 'rsvp_captcha',
				'args' => array(
					'option_name' => 'rsvp_plugin_post',
					'label_for' => 'site_key',
					'placeholder' => 'your site key'
				)
			),
			array(
				'id' => 'secret_key',
				'title' => 'Secret Key',
				'callback' => array($this->custom_settings_callbacks, 'textField'),
				'page' => 'rsvp_captcha',
				'section' => 'rsvp_captcha',
				'args' => array(
					'option_name' => 'rsvp_plugin_post',
					'label_for' => 'secret_key',
					'placeholder' => 'your sites secret key'
				)
			),
			array(
				'id' => 'blacklist_data_domain',
				'title' => 'Add New Domain <p class="description">you can upload multiple domains using comma</p>',
				'callback' => array($this->custom_settings_callbacks, 'textArea'),
				'page' => 'rsvp_blacklist',
				'section' => 'rsvp_blacklist',
				'args' => array(
					'option_name' => 'rsvp_plugin_post',
					'label_for' => 'blacklist_data_domain',
					'placeholder' => ''
				)
			),
			array(
				'id' => 'blacklist_data_subdomain',
				'title' => 'Add New Subdomain <p class="description">you can upload multiple sub domains using comma</p>',
				'callback' => array($this->custom_settings_callbacks, 'textArea'),
				'page' => 'rsvp_blacklist_subdomain',
				'section' => 'rsvp_blacklist_subdomain',
				'args' => array(
					'option_name' => 'rsvp_plugin_post',
					'label_for' => 'blacklist_data_subdomain',
					'placeholder' => ''
				)
			),
			array(
				'id' 		=> 'blacklist_data_email',
				'title' 	=> 'Add New Email <p class="description">you can upload multiple emails using comma</p>',
				'callback' 	=> array($this->custom_settings_callbacks, 'textArea'),
				'page' 		=> 'rsvp_blacklist_email',
				'section' 	=> 'rsvp_blacklist_email',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'blacklist_data_email',
					'placeholder' 	=> ''
				)
			),
			array(
				'id' 		=> 'blacklist_data_input',
				'title' 	=> 'Add New input <p class="description">you can upload multiple inputs using comma</p>',
				'callback' 	=> array($this->custom_settings_callbacks, 'textArea'),
				'page' 		=> 'rsvp_blacklist_input',
				'section' 	=> 'rsvp_blacklist_input',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'blacklist_data_input',
					'placeholder' 	=> ''
				)
			)
		);

		$this->settings->setFields( $args );
	}

	public function storeCustomPostTypes( $ID )
	{
		$post_id = $ID;
		$site_key = $_POST["site_key"];
		$secret_key = $_POST["secret_key"];
		$options = get_option('rsvp_plugin_post') ?: [];
		$options[$post_id]['site_key'] = $site_key;
		$options[$post_id]['secret_key'] = $secret_key;

		update_option( 'rsvp_plugin_post', $options );
	}

	public function registerData()
	{
		$this->storeCustomPostTypes( 'Site-Details' );
	}

	public function blacklistDomin()
	{	
		$options 	  = get_option('rsvp_plugin_post') ?: [];
		$exiting_list = $options['Site-Details']['blacklist'] ?: [];
		$added_sites  = preg_replace('/\s+/', '', $_POST['blacklist_data_domain']);
		$count = count($exiting_list) + 1;
		if (strpos($added_sites, ',') !== false) {
			$list = explode(',' , $added_sites);
			foreach ($list as $key => $value) {
				if($value != ""){
					$options['Site-Details']['blacklist'][$count] = $value;
					$count++;					
				}
			}
		}else{
			$list  = $_POST['blacklist_data_domain'];
			$options['Site-Details']['blacklist'][$count] = $list;
		}

		update_option( 'rsvp_plugin_post', $options );
	}

	public function blacklistSubdomain()
	{	
		$options 	  		= get_option('rsvp_plugin_post') ?: [];
		$exiting_list 		= $options['Site-Details']['blacklist-subdomain'] ?: [];
		$added_subdomain 	= preg_replace('/\s+/', '', $_POST['blacklist_data_subdomain']);

		$count = count($exiting_list) + 1;
		
		if (strpos($added_subdomain, ',') !== false) {
			$list = explode(',' , $added_subdomain);
			foreach ($list as $key => $value) {
				if($value != ""){
					$options['Site-Details']['blacklist-subdomain'][$count] = $value;
					$count++;					
				}
			}
		}else{
			$list  = $_POST['blacklist_data_subdomain'];
			$options['Site-Details']['blacklist-subdomain'][$count] = $list;
		}

		update_option( 'rsvp_plugin_post', $options );
	}

	public function blacklistEmail()
	{	
		$options 	  = get_option('rsvp_plugin_post') ?: [];
		$exiting_list = $options['Site-Details']['blacklist-email'] ?: [];
		$added_emails = preg_replace('/\s+/', '', $_POST['blacklist_data_email']);
		$count = count($exiting_list) + 1;
		if (strpos($added_emails, ',') !== false) {
			$list = explode(',' , $added_emails);
			foreach ($list as $key => $value) {
				if($value !=""){
					$options['Site-Details']['blacklist-email'][$count] = $value;
					$count++;
				}
			}
		}
		else{
			$list  = $_POST['blacklist_data_email'];
			$options['Site-Details']['blacklist-email'][$count] = $list;
		}

		update_option( 'rsvp_plugin_post', $options );
	}

	public function blacklistInput()
	{	
		$options 	  = get_option('rsvp_plugin_post') ?: [];
		$exiting_list = $options['Site-Details']['blacklist-input'] ?: [];
		$added_inputs = preg_replace('/\s+/', '', $_POST['blacklist_data_input']);
		$count = count($exiting_list) + 1;
		if (strpos($added_inputs, ',') !== false) {
			$list = explode(',' , $added_inputs);
			foreach ($list as $key => $value) {
				if($value !=""){
					$options['Site-Details']['blacklist-input'][$count] = $value;
					$count++;
				}
			}
		}
		else{
			$list  = $_POST['blacklist_data_input'];
			$options['Site-Details']['blacklist-input'][$count] = $list;
		}

		update_option( 'rsvp_plugin_post', $options );
	}
}
