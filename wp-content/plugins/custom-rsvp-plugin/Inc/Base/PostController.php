<?php

/**
 * @package RSVP
*/

namespace IncRsvp\Base;

use IncRsvp\Api\SettingsApi;
use IncRsvp\Base\BaseController;
use IncRsvp\Base\ShortCodeController;
use IncRsvp\Api\Callbacks\PostCallbacks;
use IncRsvp\Api\Callbacks\AdminCallbacks;

class PostController extends BaseController
{ 
	public $settings;

	public $callbacks;

	public $add_post_callbacks;

	public $shortCode;

	public $subpages 	= array();

	public $custom_post = [];

	public $imagedata;

	function register()
	{
		$this->settings 			= new SettingsApi();
		$this->callbacks 			= new AdminCallbacks();
		$this->add_post_callbacks 	= new PostCallbacks();
		$this->shortCode 			= new ShortCodeController();
		$this->setSubpages();
		$this->setSettings();
		$this->setSections();
		$this->setFields();
		$this->settings->addSubPages( $this->subpages )->register();

		if(isset($_POST['add_post'])){
			add_action('init',array( $this, 'registerPost') );
		}

		if(isset($_POST['update_post'])){
			add_action('init',array( $this, 'updatePost') );
		}
	}

	public function setSubpages()
	{
		$this->subpages = array(
	 		array(
				'parent_slug' 	=> 'custom-rsvp-plugin', 
				'page_title' 	=> 'Custom Post Type', 
				'menu_title' 	=> 'Post Manager', 
				'capability' 	=> 'manage_options', 
				'menu_slug' 	=> 'rsvp_post', 
				'callback' 		=> array( $this->callbacks, 'adminPost' )
			)
		);
	}

	public function setSettings()
	{
		$args = array(
			array(
				'option_group' 	=> 'rsvp_plugin_post_settings',
				'option_name' 	=> 'rsvp_plugin_post',
				'callback' 		=> array($this->add_post_callbacks, 'postSanitize')
			)
		);

		$this->settings->setSettings( $args );
	}

	public function setSections()
	{
		$args = array(
			array(
				'id' 			=> 'rsvp_post_index',
				'title' 		=> 'Form',
				'callback' 		=> array($this->add_post_callbacks, 'postSectionManager'),
				'page' 			=> 'rsvp_post'
			),
			array(
				'id' 			=> 'rsvp_mail',
				'title' 		=> 'Mail',
				'callback' 		=> array($this->add_post_callbacks, 'postSectionManager'),
				'page' 			=> 'rsvp_post'
			),
			array(
				'id' 			=> 'rsvp_mail_2',
				'title' 		=> '',
				'callback' 		=> array($this->add_post_callbacks, 'postSectionManager'),
				'page' 			=> 'rsvp_post'
			)
		);

		$this->settings->setSections( $args );
	}

	public function setFields()
	{
		$args = array(
			array(
				'id' 			=> 'title',
				'title' 		=> 'Title <p class="description">This is the name which will appear on the EDIT page</p>',
				'callback' 		=> array($this->add_post_callbacks, 'textField'),
				'page' 			=> 'rsvp_post',
				'section' 		=> 'rsvp_post_index',
				'args' 			=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for'		=> 'title',
					'placeholder'	=> 'Eg. Offer form',
					'required'		=> 'required'
				)
			),
			array(
				'id' 			=> 'content',
				'title' 		=> 'Form <p class="description">Edit the form html here</p>',
				'callback'		=> array($this->add_post_callbacks, 'textArea'),
				'page' 			=> 'rsvp_post',
				'section' 		=> 'rsvp_post_index',
				'args' 			=> array(
					'option_name'	=> 'rsvp_plugin_post',
					'label_for'		=> 'content',
					'placeholder'	=> '',
					'value'			=> '[text class="" id="" name="name" placeholder="enter name" minlength="" maxlength="100" required="true"]
[email class="" id="" name="email" placeholder="enter email" minlength="" maxlength="100" required="true"]
[url class="" id="" placeholder="enter website" name="website" minlength="" maxlength="100" required="true"]
[phone name="phone" class="" id="" placeholder="enter phone number" minlength="4" maxlength="17" required="true"]
[file class="" id="" placeholder="upload file" filesize="50000" name="file" filetype="pdf|doc|docx" required="true"]
[submit class="" id="" value="submit"]',
				)
			),
			array(
				'id' 		=> 'shortcode',
				'title' 	=> '<p class="description">Generate a form-tag input field.</p>',
				'callback' 	=> array($this->add_post_callbacks, 'shortCode'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_post_index',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'shortcode',
					'placeholder' 	=> 'Eg. Description'
				),
			),
			array(
				'id' 		=> 'enable_captcha',
				'title' 	=> 'Enable ReCaptcha',
				'callback' 	=> array($this->add_post_callbacks, 'checkBox'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_post_index',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'enable_captcha',
					'checked'		=> 'checked'
				),
			),
			array(
				'id' 		=> 'save_in_db',
				'title' 	=> 'Save Signups Data On Dashboard?',
				'callback' 	=> array($this->add_post_callbacks, 'checkBox'),
				'page'		=> 'rsvp_post',
				'section' 	=> 'rsvp_post_index',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'save_in_db',
					'checked'		=> ''
				),
			),
			array(
				'id' 		=> 'mail_suggestions',
				'title' 	=> '<p class="description">you can use these mail-tags to map signup forms input values:</p>',
				'callback' 	=> array($this->add_post_callbacks, 'divField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_suggestions',
					'class' 		=> 'mail-includes'
				)
			),
			array(
				'id' 		=> 'mail_subject',
				'title' 	=> 'Subject',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_subject',
					'placeholder' 	=> 'Enter subject line',
					'required'		=> 'required'
				)
			),
			array(
				'id' 		=> 'mail_to',
				'title' 	=> 'Mail To',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_to',
					'placeholder' 	=> 'disha.m@media.net',
					'required'		=> 'required'
				)
			),
			array(
				'id' 		=> 'mail_from',
				'title' 	=> 'Mail From',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_from',
					'placeholder' 	=> 'no-reply@wordpress.com',
					'value'		  	=> get_bloginfo( 'name' ).' <noreply@'.get_bloginfo( 'name' ).'>'	,
					'required'		=> 'required'
				)
			),
			array(
				'id' 		=> 'mail_body',
				'title' 	=> 'Mail Body <p class="description">you can use html tags too!</p>',
				'callback' 	=> array($this->add_post_callbacks, 'textArea'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_body',
					'placeholder' 	=> '',
					'value'			=> '',
				)
			),
			array(
				'id' 		=> 'mail_attachment',
				'title' 	=> 'Mail attachment',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_attachment',
					'placeholder' 	=> '',
					'required'		=> ''
				)
			),
			array(
				'id' 		=> 'attachment_suggestions',
				'title' 	=> '<p class="description">you can use these attachment mail-tags to map form uploads:</p>',
				'callback' 	=> array($this->add_post_callbacks, 'divField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for'		=> 'attachment_suggestions',
					'class' 		=> 'mail-includes'
				)
			),
			array(
				'id' 		=> 'mail2',	
				'title' 	=> 'Mail 2 <p class="description">Tick this to setup second email</p>',
				'callback' 	=> array($this->add_post_callbacks, 'checkBox'),
				'page'		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail2',
					'checked'		=> ''
				),
			),
			array(
				'id' 		=> 'mail_suggestions_2',
				'title' 	=> '<p class="description">you can use these mail-tags to map signup forms input values:</p>',
				'callback' 	=> array($this->add_post_callbacks, 'divField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'class' 		=> 'mail2 mail-includes',
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_suggestions_2',
				)
			),
			array(
				'id' 		=> 'mail_subject_2',
				'title' 	=> 'Subject',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'class' 		=> 'mail2',
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_subject_2',
					'placeholder' 	=> 'Enter subject line',
					'required'		=> ''
				)
			),
			array(
				'id' 		=> 'mail_to_2',
				'title' 	=> 'Mail To',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'class' 		=> 'mail2',
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_to_2',
					'placeholder' 	=> 'disha.m@media.net',
					'required'		=> ''
				)
			),
			array(
				'id' 		=> 'mail_from_2',
				'title' 	=> 'Mail From',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'class' 		=> 'mail2',
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_from_2',
					'placeholder' 	=> 'no-reply@wordpress.com',
					'value'		  	=> get_bloginfo( 'name' ).' <noreply@'.get_bloginfo( 'name' ).'>'	,
					'required'		=> ''
				)
			),
			array(
				'id' 		=> 'mail_body_2',
				'title' 	=> 'Mail Body <p class="description">you can use html tags too!</p>',
				'callback' 	=> array($this->add_post_callbacks, 'textArea'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'class' 		=> 'mail2',
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_body_2',
					'placeholder' 	=> '',
					'value'			=> '',
				)
			),
			array(
				'id' 		=> 'mail_attachment_2',
				'title' 	=> 'Mail attachment',
				'callback' 	=> array($this->add_post_callbacks, 'textField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'class' 		=> 'mail2',
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for' 	=> 'mail_attachment_2',
					'placeholder' 	=> '',
					'required'		=> ''
				)
			),
			array(
				'id' 		=> 'attachment_suggestions_2',
				'title' 	=> '<p class="description">you can use these attachment mail-tags to map form uploads:</p>',
				'callback' 	=> array($this->add_post_callbacks, 'divField'),
				'page' 		=> 'rsvp_post',
				'section' 	=> 'rsvp_mail_2',
				'args' 		=> array(
					'class' 		=> 'mail2 mail-includes',
					'option_name' 	=> 'rsvp_plugin_post',
					'label_for'		=> 'attachment_suggestions_2',
				)
			),			
		);

		$this->settings->setFields( $args );
	}

	public function storeCustomPostTypes( $ID )
	{
		$post_id 			= $ID;
		$title 				= $_POST["title"];
		$content 			= stripslashes($_POST["content"]);
		$chaptcha 			= ($_POST['enable_captcha'] == 'on') ? 'checked' : '';
		$save_data 			= ($_POST['save_in_db'] == 'on') ? 'checked' : '';
		$options 			= get_option('rsvp_plugin_post') ?: [];

		$mail_to			= $_POST['mail_to'];
		$mail_from			= $_POST['mail_from'];
		$mail_body			= stripslashes($_POST['mail_body']);
		$mail_subject		= stripslashes($_POST['mail_subject']);
		$mail_attachment	= $_POST['mail_attachment'];

		$mail2 				= ($_POST['mail2'] == 'on') ? 'checked' : '';
		$mail_to_2			= $_POST['mail_to_2'];
		$mail_from_2		= $_POST['mail_from_2'];
		$mail_body_2		= stripslashes($_POST['mail_body_2']);
		$mail_subject_2		= stripslashes($_POST['mail_subject_2']);
		$mail_attachment_2	= $_POST['mail_attachment'];

		$options[$post_id] 	= array(
			'ID' 				=> $post_id,
			'title' 			=> $title,
			'content'   		=> $content,
			'date'				=> date('Y-m-d'),
			'enable_captcha'	=> $chaptcha,
			'save_in_db'		=> $save_data,
			'mail_to'			=> $mail_to,
			'mail_from'			=> $mail_from,
			'mail_body'			=> $mail_body,
			'mail_subject'		=> $mail_subject,
			'mail_attachment'	=> $mail_attachment,
			'mail2'				=> $mail2,
			'mail_to_2'			=> $mail_to_2,
			'mail_from_2'		=> $mail_from_2,
			'mail_body_2'		=> $mail_body_2,
			'mail_subject_2'	=> $mail_subject_2,
			'mail_attachment_2'	=> $mail_attachment_2,
		);

		update_option( 'rsvp_plugin_post', $options );
	}

	public function registerPost()
	{
		$post_id = substr(uniqid(rand(), true), 4, 4);

		$this->storeCustomPostTypes( $post_id );

		$this->shortCode->shortCode( $post_id );
	}

	public function updatePost()
	{
		if(isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) {
			$ID = $_POST["update_post"];			
			$this->storeCustomPostTypes( $ID );
		}
	}
}