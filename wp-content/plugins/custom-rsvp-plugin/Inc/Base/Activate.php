<?php

/**
 * @package RSVP
*/

namespace IncRsvp\Base;

class Activate
{	
	public static function activate()
	{
		flush_rewrite_rules();
	
		$default = [];

		if ( ! get_option( 'rsvp_plugin_post' ) ) {
			add_option( 'rsvp_plugin_post', $default );
	    	global $wpdb;
		    if (!empty($wpdb->charset)) {
		        $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		    }
		    if (!empty($wpdb->collate)) {
		        $charset_collate .= " COLLATE {$wpdb->collate}";
		    }
		    require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		    $table_name = '{wpdb->base_prefix}rsvp_signups';
	    	$create_ddl = "CREATE TABLE IF NOT EXISTS `{$wpdb->base_prefix}rsvp_signups` (
	    		  ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			      form_id int(20) NOT NULL,
			      form_data varchar(255) NOT NULL,
			      user_IP varchar(255) NOT NULL,
			      signup_date datetime NOT NULL
			    ) $charset_collate;";	

		    return maybe_create_table($table_name,$create_ddl);
		}

	}
}

