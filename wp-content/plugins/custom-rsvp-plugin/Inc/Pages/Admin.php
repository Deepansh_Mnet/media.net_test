<?php

/**
 * 
*/
namespace IncRsvp\Pages;

use IncRsvp\Api\SettingsApi;
use IncRsvp\Base\BaseController;
use IncRsvp\Api\Callbacks\AdminCallbacks;

class Admin extends BaseController
{
	public $settings;

	public $pages = array();

	public $subpages = array();

	public function register() 
	{
		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->pages = array(
			array(
				'page_title' => 'RSVP', 
				'menu_title' => 'RSVP', 
				'capability' => 'publish_pages', 
				'menu_slug' => 'rsvp_plugin', 
				'callback' => array( $this->callbacks, 'adminDashboard'),
				'icon_url' => 'dashicons-store', 
				'position' => 110
			)
		);

		$this->subpages = array(
			array(
				'parent_slug' => 'rsvp_plugin', 
				'page_title' => 'Add New', 
				'menu_title' => 'Add New', 
				'capability' => 'manage_options', 
				'menu_slug' => 'rsvp_plugin_new', 
				'callback' => array( $this->callbacks, 'addNewForms'),
				// 'callback' => function(){echo '<h1>RSVP Setting</h1>'; },
			),
			array(
				'parent_slug' => 'rsvp_plugin', 
				'page_title' => 'Settings', 
				'menu_title' => 'Settings', 
				'capability' => 'manage_options', 
				'menu_slug' => 'rsvp_plugin_settings', 
				'callback' => array( $this->callbacks, 'settings'),
				// 'callback' => function(){echo '<h1>RSVP Setting</h1>'; },
			)
		);
		
		$this->settings->addPages( $this->pages )->withSubPage( 'Dashboard' )->addSubPages( $this->subpages )->register();
	}
}