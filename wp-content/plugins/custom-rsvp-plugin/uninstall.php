  
<?php


if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	die;
}

// Clear Database stored data
$rsvp_forms = get_posts( array( 'post_type' => 'Rsvp Form', 'numberposts' => -1 ) );
delete_option( 'rsvp_plugin_post' );
foreach( $rsvp_forms as $rsvp_form ) {
	wp_delete_post( $rsvp_form->ID, true );
}

// Access the database via SQL
global $wpdb;
$table_name = $wpdb->prefix . 'rsvp_signups';
$wpdb->query( "DELETE FROM wp_posts WHERE post_type = 'rsvp_form'" );
$wpdb->query( "DELETE FROM wp_postmeta WHERE post_id NOT IN (SELECT id FROM wp_posts)" );
$wpdb->query( "DELETE FROM wp_term_relationships WHERE object_id NOT IN (SELECT id FROM wp_posts)" );
$wpdb->query("DROP TABLE IF EXISTS $table_name");