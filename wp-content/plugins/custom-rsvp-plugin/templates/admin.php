<?php 
   $options = get_option('rsvp_plugin_post') ?: [];
   global $wpdb;
?>
<div class="wrap signup-plugin <?php echo isset($_POST["edit_post"]) ? 'd-none' : '' ?>">
   <h1 class="wp-heading-inline">Signup Settings</h1>
   <div class="nav-tabs">
      <input class="tab-inputs" id="tab1" type="radio" name="nav-tabs">
      <label class="labels" for="tab1">
         Recaptcha Settings
      </label>
      <input class="tab-inputs" id="tab2" type="radio" name="nav-tabs" checked>
      <label class="labels" for="tab2">
         Black Listed Websites
      </label>
      <div class="content">
         <div id="content1" class="active">
            <div class="accordion">
               <div class="tabs">
                 <div class="tab">
                     <input type="checkbox" id="chck4" class="accordion-input" checked>
                     <label class="tab-label" for="chck4">Captcha keys</label>
                     <div class="tab-content">
                        <p class="description">
                           Genrate you site key and secrect key by adding you site 
                           <a href="https://www.google.com/recaptcha/admin" target="_blank">Here</a>
                        </p>
                        <br>
                        <form method="post" action="options.php">
                           <?php 
                              settings_fields( 'rsvp_captcha_settings' );
                              do_settings_sections( 'rsvp_captcha' );
                              submit_button(__( 'Save', 'textdomain' ), 'primary', 'add_setting_data', false);
                           ?>
                        </form>
                     </div>
                 </div>
               </div>            
            </div>
         </div>
         <div id="content2">
            <?php 
               $blacklist_sites     = $options['Site-Details']['blacklist'];
               $blacklist_subdomain = $options['Site-Details']['blacklist-subdomain'];
               $blacklist_email     = $options['Site-Details']['blacklist-email'];
               $blacklist_input     = $options['Site-Details']['blacklist-input'];
            ?>

            <div class="accordion">
               <div class="tabs">
                 <div class="tab">
                     <input type="checkbox" id="chck0" class="accordion-input" checked>
                     <label class="tab-label" for="chck0">
                        <strong>List of Banned Inputs</strong>
                     </label>
                     <div class="tab-content no-border">
                        <form method="post" action="options.php" class="mb-10">
                           <?php 
                              if (!empty($blacklist_input)) {
                                 echo '
                                    <table class="wp-list-table widefat fixed table-view-list striped input">
                                       <thead>
                                          <tr>
                                             <th scope="col" id="sr" class="manage-column column-sr column-primary "><strong>Sr</strong></th>
                                             <th scope="col" id="input" class="manage-column column-input"><strong>input</strong></th>
                                             <th scope="col" id="action" class="manage-column column-action"><strong>Action</strong></th>
                                          </tr>
                                       </thead>
                                       <tbody id="the-list" data-wp-lists="list:input"> 
                                 ';
                                 $i=1;
                                    foreach ($blacklist_input as $inputs => $input) {
                                       echo "<tr><td>{$i}</td>";
                                       echo "<td>{$input}</td>";
                                       echo "<td> <div>";
                                          echo "<label for=".$inputs." class='button delete link-btn'>Delete</label>";
                                          submit_button($inputs,'delete hidden primary small','remove_input',false,array('id' => $inputs));
                                          echo "";
                                          $i++;
                                    }
                                 echo '</div> </td></tr></tbody>
                                 </table><br>';
                              }else{
                                 echo "<h4 class='text-center sub-title'>No Blacklisted inputs Added Yet!</h4>";
                              }
                              settings_fields( 'rsvp_blacklist_input_settings' );
                              do_settings_sections( 'rsvp_blacklist_input' );
                              echo '<div class="text-right">';
                              submit_button(__( 'Add New input', 'textdomain' ), 'primary large', 'add_blacklist_input', false);
                              echo '</div>';
                           ?>
                        </form>
                        <br>
                     </div>
                 </div>
                 <div class="tab">
                     <input type="checkbox" id="chck1" class="accordion-input" checked>
                     <label class="tab-label" for="chck1">
                        <strong>List of blacklisted Email</strong>
                     </label>
                     <div class="tab-content no-border">
                        <form method="post" action="options.php" class="mb-10">
                           <?php 
                              if (!empty($blacklist_email)) {
                                 echo '
                                    <table class="wp-list-table widefat fixed table-view-list striped email">
                                       <thead>
                                          <tr>
                                             <th scope="col" id="sr" class="manage-column column-sr column-primary "><strong>Sr</strong></th>
                                             <th scope="col" id="email" class="manage-column column-email"><strong>Email</strong></th>
                                             <th scope="col" id="action" class="manage-column column-action"><strong>Action</strong></th>
                                          </tr>
                                       </thead>
                                       <tbody id="the-list" data-wp-lists="list:email"> 
                                 ';
                                 $i=1;
                                    foreach ($blacklist_email as $emails => $email) {
                                       echo "<tr><td>{$i}</td>";
                                       echo "<td>{$email}</td>";
                                       echo "<td> <div>";
                                          echo "<label for=".$emails." class='button delete link-btn'>Delete</label>";
                                          submit_button($emails,'delete hidden primary small','remove_email',false,array('id' => $emails));
                                          echo "";
                                          $i++;
                                    }
                                 echo '</div> </td></tr></tbody>
                                 </table><br>';
                              }else{
                                 echo "<h4 class='text-center sub-title'>No Blacklisted Emails Added Yet!</h4>";
                              }
                              settings_fields( 'rsvp_blacklist_email_settings' );
                              do_settings_sections( 'rsvp_blacklist_email' );
                              echo '<div class="text-right">';
                              submit_button(__( 'Add New Email', 'textdomain' ), 'primary large', 'add_blacklist_email', false);
                              echo '</div>';
                           ?>
                        </form>
                        <br>
                     </div>
                 </div>
                 <div class="tab">
                     <input type="checkbox" id="chck2" class="accordion-input" checked>
                     <label class="tab-label" for="chck2">
                        <strong>List of blacklisted Sub Domains</strong>
                     </label>
                     <div class="tab-content no-border">
                        <form method="post" action="options.php" class="mb-10">
                           <?php 
                              if (!empty($blacklist_subdomain)) {
                                 echo '
                                    <table class="wp-list-table widefat fixed table-view-list striped subdomain">
                                       <thead>
                                          <tr>
                                             <th scope="col" id="sr" class="manage-column column-sr column-primary "><strong>Sr</strong></th>
                                             <th scope="col" id="subdomain" class="manage-column column-subdomain"><strong>Subdomain</strong></th>
                                             <th scope="col" id="action" class="manage-column column-action"><strong>Action</strong></th>
                                          </tr>
                                       </thead>
                                       <tbody id="the-list" data-wp-lists="list:subdomain"> 
                                 ';
                                 $i=1;
                                    foreach ($blacklist_subdomain as $subdomains => $subdomain) {
                                       echo "<tr><td>{$i}</td>";
                                       echo "<td>{$subdomain}</td>";
                                       echo "<td> <div>";
                                          echo "<label for=".$subdomains." class='button delete link-btn'>Delete</label>";
                                          submit_button($subdomains,'delete hidden primary small','remove_subdomain',false,array('id' => $subdomains));
                                          echo "";
                                          $i++;
                                    }
                                 echo '</div> </td></tr></tbody>
                                 </table><br>';
                              }else{
                                 echo "<h4 class='text-center sub-title'>No Blacklisted subdomains Added Yet!</h4>";
                              }
                                 settings_fields( 'rsvp_blacklist_subdomin_settings' );
                                 do_settings_sections( 'rsvp_blacklist_subdomain' );
                                 echo '<div class="text-right">';
                                 submit_button(__( 'Add New Subdomain', 'textdomain' ), 'primary large', 'add_blacklist_subdomain', false);
                                 echo '</div>';
                           ?>
                        </form>
                        <br>
                     </div>
                 </div>
                 <div class="tab">
                     <input type="checkbox" id="chck3" class="accordion-input" checked>
                     <label class="tab-label" for="chck3">
                        <strong>List of blacklisted Domains</strong>
                     </label>
                     <div class="tab-content no-border">
                        <form method="post" action="options.php" class="mb-10">
                           <?php 
                              if (!empty($blacklist_sites)) {
                                 echo '
                                    <table class="wp-list-table widefat fixed table-view-list striped domain">
                                       <thead>
                                          <tr>
                                             <th scope="col" id="sr" class="manage-column column-sr column-primary "><strong>Sr</strong></th>
                                             <th scope="col" id="domain" class="manage-column column-domain"><strong>Domain</strong></th>
                                             <th scope="col" id="action" class="manage-column column-action"><strong>Action</strong></th>
                                          </tr>
                                       </thead>
                                       <tbody id="the-list" data-wp-lists="list:domain"> 
                                 ';
                                 $i=1;
                                    foreach ($blacklist_sites as $domains => $domain) {
                                       echo "<tr><td>{$i}</td>";
                                       echo "<td>{$domain}</td>";
                                       echo "<td> <div>";
                                          echo "<label for=".$domains." class='button delete link-btn'>Delete</label>";
                                          submit_button($domains,'delete hidden primary small','remove_domain',false,array('id' => $domains));
                                          echo "";
                                          $i++;
                                    }
                                 echo '</div> </td></tr></tbody>
                                 </table><br>';
                              }else{
                                 echo "<h4 class='text-center sub-title'>No Blacklisted domains Added Yet!</h4>";
                              }
                                 settings_fields( 'rsvp_blacklist_settings' );
                                 do_settings_sections( 'rsvp_blacklist' );
                                 echo '<div class="text-right">';
                                 submit_button(__( 'Add New Domain', 'textdomain' ), 'primary large', 'add_blacklist_data', false);
                                 echo '</div>';
                           ?>
                        </form>
                        <br>
                     </div>
                 </div>
               </div>            
            </div>
         </div>
      </div>
   </div>
</div>