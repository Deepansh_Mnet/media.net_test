<form id="rsvp-form" action="#" method="post" 
	data-url=<?php echo admin_url('admin-ajax.php') ?>>
	<input type="text"  name="name"><br>
	<p data-error="invalidEmail" class="error">Invalid</p>
	<input type="text" name="email"><br>
	<p data-error="invalidName" class="error">Invalid</p>
	<input type="text" name="message"><br>
	<p data-error="invalidMessage" class="error">Invalid</p>
	<input type="submit" name="rsvp_submit"><br>
</form>

<div class="field-wrapper">
    <input type="text" placeholder="Your Website" name="website" id="website" tabindex="1" autocomplete="off" class="form-control"  />
</div>  
<div class="field-wrapper">
    <input type="text" tabindex="2" id="phone_no" name="phone_no" class="form-control" autocomplete="off" placeholder="Phone Number" />
</div>
<div class="field-wrapper">
    <input type="text" id="email" tabindex="3" name="email" class="email-add form-control textbox su-input inputbox" autocomplete="off" placeholder="Email Address" />
</div>
<button type="submit" class="btn submit-button btn-primary invalid">Get Started
    <div class="loader"><div></div><div></div><div></div></div> 
</button>