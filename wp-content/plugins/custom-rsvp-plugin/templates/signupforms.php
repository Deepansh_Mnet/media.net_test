<?php 
   $options = get_option('rsvp_plugin_post') ?: [];
   global $wpdb;
?>
<div class="wrap signup-plugin <?php echo (isset($_POST["edit_post"]) || isset($_POST["view_signups"])) ? 'd-none' : '' ?>">
   <h1 class="wp-heading-inline">Signup Forms</h1>
   <a href="<?php echo admin_url("admin.php?page=rsvp_plugin_new") ?>" class="page-title-action">Add New</a>
   <br><br><br>
   <table class="wp-list-table widefat striped table-view-list posts">
      <thead>
         <tr>
            <th scope="col" id="title" class="manage-column column-title column-primary">Title</th>
            <th scope="col" id="post-id" class="manage-column column-post-id">Post ID</th>
            <th scope="col" id="shortcode" class="manage-column column-shortcode">Shortcode</th>
            <th scope="col" id="signups" class="manage-column column-signups">Total Signups</th>
            <th scope="col" id="date" class="manage-column column-date">Date</th>
            <th scope="col" id="action" class="manage-column column-action">Action</th>
         </tr>
      </thead>
      <tbody id="the-list" data-wp-lists="list:post">
         <?php 
            $form_count = $options;
            unset($form_count['Site-Details']);
            if (!empty($form_count)) {
               foreach ($options as $items) {
                  if ( !empty($items['ID']))
                  {
                     $ID = $items['ID'];
                     $get_signups = $wpdb->get_results("SELECT form_id,COUNT(*) as total_signups FROM `wp_rsvp_signups` WHERE form_id = ".$items['ID'].""); 
                     $title = $options[$ID]['title'];
                     $date = $options[$ID]['date'];
                     echo '
                        <tr>
                           <td class="title column-title has-row-actions column-primary" data-colname="title">
                              '.$title;
                              if( current_user_can('administrator') )
                                 {
                                    echo '
                                          <div class="row-actions">
                                             <span class="edit">
                                                <form method="post" action="" class="inline">
                                                   <input type="hidden" value="'.$ID.'" name="edit_post">
                                                   ';
                                                   submit_button('Edit','link-btn','submit',false);
                                                   echo ' | 
                                                </form>
                                             </span>
                                             <span class="delete">
                                                <form method="post" action="options.php" class="inline">
                                                   <input type="hidden" value="'.$ID.'" name="remove_post">
                                                   ';
                                                   settings_fields( 'rsvp_plugin_post_settings' );
                                                   if( !(get_post_status($ID) == 'trash') ){
                                                      submit_button('Delete','delete link-btn','submit',false);
                                                   };
                                                   echo '
                                                </form>
                                             </span>
                                          </div>
                                    ';
                                 }
                        echo '
                           </td>
                           <td class="'.$ID.' column-'.$ID.' has-row-actions column-primary" data-colname="'.$ID.'">
                              '.$ID.'
                           </td>
                           <td class="shortcode column-shortcode has-row-actions column-primary" data-colname="shortcode">
                              <code>[signupform id=\''.$ID.'\']</code>
                           </td>
                           <td class="total_signups column-total-signups has-row-actions column-primary" data-colname="total_signups">
                              '.$get_signups[0]->total_signups.'</td>
                           <td class="date column-total-signups has-row-actions column-primary" data-colname="date">
                              '.$date.'
                           </td>
                           <td class="view_signups column-view-signups has-row-actions column-primary" data-colname="view_signups">
                           ';
                           echo "<form method='post' action=''> <input type='hidden' id='' value='".$ID."' name='view_signups'>";
                           submit_button('View Sign Ups','small', 'submit_post',false);                           
                           echo '<input type="hidden" id="" value="'.$ID.' name="view_signups"></form>
                           </td>
                        </tr>
                     ';
                  }
               }
            }else{
               echo "<tr><td colspan='6'><p class='text-center'>No Forms Added Yet!</p></td></tr>";
            }
         ?>      
      </tbody>
      <tfoot>
         <tr>
            <th scope="col" id="title" class="manage-column column-title column-primary ">Title</th>
            <th scope="col" id="post-id" class="manage-column column-post-id">Post ID</th>
            <th scope="col" id="shortcode" class="manage-column column-shortcode">Shortcode</th>
            <th scope="col" id="signups" class="manage-column column-signups">Total Signups</th>
            <th scope="col" id="date" class="manage-column column-date">Date</th>
            <th scope="col" id="action" class="manage-column column-action">Action</th>
         </tr>         
      </tfoot>
      </table>
</div>
<div class="wrap signup-plugin <?php echo isset($_POST["edit_post"]) ? '' : 'd-none' ?>">
   <?php 
         $ID = $_POST["edit_post"];
         echo '<h1 class="wp-heading-inline">Edit '.$options[$ID]["title"].'</h1>';
   ?>
   <a href="<?php echo admin_url("admin.php?page=rsvp_plugin_new") ?>" class="page-title-action">Add New</a>
   <form method="post" action="options.php">
      <?php 
         settings_fields( 'rsvp_plugin_post_settings' );
         do_settings_sections( 'rsvp_post' );
         if(isset($_POST["edit_post"])){
            wp_nonce_field('post_nonce', 'post_nonce_field');
            echo "<input type='hidden' value='".$ID."' name='update_post'>";
            submit_button(__( 'Update Form', 'textdomain' ), 'primary', false);
         }else{
            submit_button(__( 'Add Form', 'textdomain' ), 'primary', 'add_post', false);
         }
      ?>
   </form>
</div>
<div class="wrap signup-plugin <?php echo isset($_POST["view_signups"]) ? '' : 'd-none' ?>">
   <?php 
      $ID = $_POST["view_signups"];
      $post_title = get_the_title( $ID );
      $args = array(
          'post_id' => $ID,
      );
      echo '<h1 class="wp-heading-inline">Signups for '.$options[$ID]["title"].'</h1>';
      $comments = $wpdb->get_results("SELECT * FROM `wp_rsvp_signups` WHERE form_id = ".$ID."");
      $other_fields = $wpdb->get_col("SELECT `form_data` FROM `wp_rsvp_signups` WHERE form_id = ".$ID."");

      if(count($comments) != 0){
         foreach ($other_fields as $key => $other_field) 
         {
            $other_fields_data = unserialize($other_field);
            $other_fields_data['ip'] = $comments[$key]->user_IP;
            $other_fields_data['date'] = $comments[$key]->signup_date;
            $form_data_table[] = $other_fields_data;

         }
         echo '<div class="text-right">';
         submit_button(__( 'Download All'), 'primary export', 'download', false );
         echo "<br><br></div><table class='wp-list-table widefat fixed striped table-view-list  view-signups details'><thead><tr>";
         foreach(array_keys($other_fields_data) as $table_headings){
            echo '<th><strong>'. $table_headings . '</strong></th>';
         }
         echo '</tr></thead><tbody id="the-list" data-wp-lists="list:post">';

         foreach($form_data_table as $table_data )
         {
            foreach($table_data as $table_key => $table_value )
               echo "<td>". str_replace('\\', '', $table_value)."</td>";
            echo "</tr>";
         }
            echo "</tr></tbody></table>";

         }else{
            echo "<h2 class='text-center'>No signups Yet!</h2>";
         }
   ?>
</div>