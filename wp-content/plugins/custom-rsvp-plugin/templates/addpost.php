<div class="wrap">
   <?php 
      $options = get_option('rsvp_plugin_post') ?: [];
      global $wpdb;
   ?>
   <div class="nav-tabs">
      <input class="tab-inputs" id="tab1" type="radio" name="nav-tabs" <?php echo ! isset($_POST["edit_post"]) ? 'checked' : '' ?> >
      <label class="labels <?php echo (sizeof($options) < 1) ? 'd-none' : '' ?>" for="tab1"> <h4>All Forms</h4> </label>
      
      <?php if( current_user_can('administrator') ){ ?>
      <input class="tab-inputs" id="tab2" type="radio" name="nav-tabs" <?php echo ( sizeof($options) < 1 || isset($_POST["edit_post"])) ? 'checked' : '' ?> >
      <label class="labels" for="tab2"> 
         <h4> <?php echo isset($_POST["edit_post"]) ? "Edit" : "Add New" ?> Form</h4> 
      </label>
      <?php } ?>

      <input class="tab-inputs" id="tab3" type="radio" name="nav-tabs" <?php echo isset($_POST["view_signups"]) ? 'checked' : '' ?> disabled>
      <label class="labels <?php echo (sizeof($options) < 1) ? 'd-none' : '' ?>" for="tab3"> <h4>Export</h4> </label>

      <div class="content">
         <div id="content1" class="">
            <?php 
               if (!empty($options)) {
                  echo "<h3>Manage Your Custom Forms</h3>";
                  echo "<table class='table'><tr> <th>Post Id</th> <th>Title</th> <th>Date</th><th>Total Signups</th><th>Short code</th> <th>Actions</th >";
                     foreach ($options as $items) {
                        if ( !empty($items['ID']))
                        {
                           $ID = $items['ID'];
                           $get_signups = $wpdb->get_results("SELECT form_id,COUNT(*) as total_signups FROM `wp_rsvp_signups` WHERE form_id = ".$items['ID'].""); 
                           $title = $options[$ID]['title'];
                           $date = $options[$ID]['date'];
                           echo "<tr><td>{$ID}</td>
                           <td>{$title}</td>
                           <td>{$date}</td>
                           <td> <div> {$get_signups[0]->total_signups} </div> </td>
                           <td> <div> <code> [signupform id='{$ID}']</code> </div> </td>";
   
                           echo "<td> <div><form method='post' action='' class='inline'>";
                           echo "<input type='hidden' id='' value='".$ID."' name='view_signups'>";
                           submit_button('View Sign Ups','primary small', 'submit_post',false);
   
                           if( current_user_can('administrator') )
                           {
                              echo "</form> <span> | </span>";
                              echo "<form method='post' action='' class='inline'>";
                              echo "<input type='hidden' value='".$ID."' name='edit_post'>";
                              submit_button('Edit','primary small','submit',false);
                              echo "</form> <span> | </span>";
   
                              echo "<form method='post' action='options.php' class='inline'>";
                              settings_fields( 'rsvp_plugin_post_settings' );
                              echo "<input type='hidden' value='".$ID."' name='remove_post'>";
                              if( !(get_post_status($ID) == 'trash') ){
                                 submit_button('Delete','delete primary small','submit',false);
                              };
                              echo "</form>";
                           }
                        }
                     }
                  echo "</form></div> </td></tr></table>";
               }else{
                  echo "<h2 class='text-center'>No Forms Added Yet!</h2>";
               }
            ?> 
         </div>
         <div id="content2"  class="">
            <form method="post" action="options.php">
               <?php 
                  settings_fields( 'rsvp_plugin_post_settings' );
                  do_settings_sections( 'rsvp_post' );
                  if(isset($_POST["edit_post"])){
                     $ID = $_POST["edit_post"];
                     wp_nonce_field('post_nonce', 'post_nonce_field');
                     echo "<input type='hidden' value='".$ID."' name='update_post'>";
                     submit_button(__( 'Update Form', 'textdomain' ), 'primary', false);
                  }else{
                     submit_button(__( 'Add Form', 'textdomain' ), 'primary', 'add_post', false);
                  }
               ?>
            </form>
         </div>

         <div id="content3">
            <?php 
               if(isset($_POST["view_signups"])){
                  $ID = $_POST["view_signups"];
                  $post_title = get_the_title( $ID );
                  $args = array(
                      'post_id' => $ID,
                  );
                  $comments = $wpdb->get_results("SELECT * FROM `wp_rsvp_signups` WHERE form_id = ".$ID."");
                  $other_fields = $wpdb->get_col("SELECT `form_data` FROM `wp_rsvp_signups` WHERE form_id = ".$ID."");

                  if(count($comments) != 0){
                     foreach ($other_fields as $other_field) 
                     {
                        $other_fields_data = unserialize($other_field);
                        foreach ($comments as $comment) 
                           {
                              $other_fields_data['ip'] = $comment->user_IP;
                              $other_fields_data['date'] = $comment->signup_date;
                           }
                        $form_data_table[] = $other_fields_data; 
                     }

                     echo "<div class='head'><h2>List of Sign Ups for <?php echo $post_title; ?></h2>";
                     
                     submit_button(__( 'Download All'), 'primary export', 'download', false );
                     
                     echo "</div><table class='details table'><tr>";


                     foreach(array_keys($other_fields_data) as $table_headings){
                        echo '<th>'. $table_headings . '</th>';
                     }

                     echo "</tr>";

                     foreach($form_data_table as $table_data )
                     {
                        foreach($table_data as $table_key => $table_value )
                           echo "<td>". $table_value ."</td>";
                        echo "</tr>";
                     }
                        echo "</tr></table>";

                     }else{
                        echo "<h2 class='text-center'>No signups Yet!</h2>";
                     }
               }
            ?>
         </div>
      </div>

   </div>
</div>