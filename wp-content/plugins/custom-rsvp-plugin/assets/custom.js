var rsvp_js = $.noConflict();
rsvp_js(function() {
	showMail2();
	rsvp_js('#mail2').on('change',function () {
		showMail2();
	});
	function showMail2() {
		if(rsvp_js('#mail2').is(':checked')){
			rsvp_js('.mail2').show('slow');
		}else{
			rsvp_js('.mail2').hide('slow');
		}
	}
	function download_csv(csv, filename) {
	    var csvFile;
	    var downloadLink;
	    csvFile = new Blob([csv], {type: "text/csv"});
	    downloadLink = document.createElement("a");
	    downloadLink.download = filename;
	    downloadLink.href = window.URL.createObjectURL(csvFile);
	    downloadLink.style.display = "none";
	    document.body.append(downloadLink);
	    downloadLink.click();
	    downloadLink.remove();
	}
	function export_table_to_csv(filename) {
		var csv = [];
		var rows = document.querySelectorAll(".details tr");
	    var html = document.querySelector(".details").outerHTML;
	    for (var i = 0; i < rows.length; i++) {
			var row = [], cols = rows[i].querySelectorAll("td, th");
	        for (var j = 0; j < cols.length; j++) 
	            row.push(cols[j].innerText);
			csv.push(row.join(","));		
		}
	    download_csv(csv.join("\n"), filename);
	}
	function mail_shortcodes(){
		// var regex = /(?:\[)(?:^f)(?:.*)(?:name=")(.*?)"/g;
		var mail_code = [];
		var regex = /(?:\[)(?:[^fil])(?:.*)(?:name=")(.*?)"/gm;
		var value = rsvp_js('#content').val();
		var match;
		do {
		    match = regex.exec(value);
		    if (match) {
			    if(rsvp_js.inArray( '['+match[1]+']', mail_code ) == -1){
					mail_code.push('['+match[1]+']');
			    	rsvp_js('#mail_suggestions').text(mail_code.join(' '));
			    	rsvp_js('#mail_suggestions_2').text(mail_code.join(' '));
			    }
		    }
		} while (match);
	}

	mail_shortcodes();

	function attchemnet_shortcodes(){
		var attch_code = [];
		var regex = /\[file.*name="(.*?)"/g;
		var value = rsvp_js('#content').val();
		var match;
		do {
		    match = regex.exec(value);
		    if (match) {
			    if(rsvp_js.inArray( '['+match[1]+']', attch_code ) == -1){
					attch_code.push('['+match[1]+']');
			    	rsvp_js('#attachment_suggestions').text(attch_code.join(' '));
			    	rsvp_js('#attachment_suggestions_2').text(attch_code.join(' '));
			    }
		    }
		} while (match);
	}
	
	attchemnet_shortcodes();

    rsvp_js(".export").on('click', function(){
    	export_table_to_csv('signup.csv');
    });

    rsvp_js(".shotcode").on('click', function() {
        var code = rsvp_js(this).attr('data-value');
        rsvp_js('.popup-tag-output').val(code);
    })

    rsvp_js('.popup-fields').on('change focus keyup',function(){
    	var attr 			= rsvp_js(this).attr('data-value');
    	var input 			= rsvp_js(this).val();
    	var output 			= rsvp_js(".popup-tag-output");
    	var output_value 	= rsvp_js(".popup-tag-output").val();
    	var replace 		= output_value.match(new RegExp('(?:'+attr+'=")(.*?)(?:")'))[0];
		output.val(output_value.replace(replace,attr+'="'+input+'"'));
    })

    rsvp_js('input#required-attr').on('change',function(){
    	var attr 			= rsvp_js(this).attr('data-value');
    	var output 			= rsvp_js(".popup-tag-output");
    	var output_value 	= rsvp_js(".popup-tag-output").val();
    	var input 			= rsvp_js(this).val();
    	if(!this.checked) {
    		output.val(output_value.replace('required="true"','required="false"'));
    	}else{
    		output.val(output_value.replace('required="false"','required="true"'));
    	}
    })

    rsvp_js('#content').on('change keyup',function(){
		mail_shortcodes();
		attchemnet_shortcodes();
	});


    rsvp_js(".popup-submit").on('click', function() {
        var txtarea = rsvp_js("#content")[0];
        var text = rsvp_js('.popup-tag-output').val();
		if (!txtarea) {
			return;
		}

		var scrollPos = txtarea.scrollTop;
		var strPos = 0;
		var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
		"ff" : (document.selection ? "ie" : false));
		if (br == "ie") {
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart('character', -txtarea.value.length);
		strPos = range.text.length;
		} else if (br == "ff") {
		strPos = txtarea.selectionStart;
		}

		var front = (txtarea.value).substring(0, strPos +"\n");
		var back = (txtarea.value).substring(strPos, txtarea.value.length +"\n");
		txtarea.value = front + text + back;
		strPos = strPos + text.length;
		if (br == "ie") {
		txtarea.focus();
		var ieRange = document.selection.createRange();
		ieRange.moveStart('character', -txtarea.value.length);
		ieRange.moveStart('character', strPos);
		ieRange.moveEnd('character', 0);
		ieRange.select();
		} else if (br == "ff") {
		txtarea.selectionStart = strPos;
		txtarea.selectionEnd = strPos;
		txtarea.focus();
		}

		txtarea.scrollTop = scrollPos;

		rsvp_js('.popup-content input').val('');

		mail_shortcodes();
		attchemnet_shortcodes();
    });

});