$(function() {
	$('.rsvp-form input[type="submit"]').after('<span class="loading"></span>');
	var rsvpForm = document.getElementById('rsvp-form');
	if(rsvpForm != '' || rsvpForm != null){
		var url = rsvpForm.dataset.url;
	}
	var params = new FormData(rsvpForm);
	
	var file_data,form_data;

    $('#rsvp-file').on('change',function(){
	    file_data = $('#rsvp-file').prop('files')[0];
	    form_data = new FormData();
	    form_data.append('file', file_data);
	    form_data.append('action', 'file_upload');
    })

	$.validator.methods.text = function( value, element ) {
		var textRegex = /^[A-Za-z0-9 ']+$/;
	    return this.optional(element) || (!!value.trim().toLocaleLowerCase().match(textRegex));
	}

	$.validator.methods.email = function( value, element ) {
		var emailRegex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,14}|[0-9]{1,3})(\]?)$/;
		var domain = value.split('@')[1];
	    return this.optional(element) || (!!value.trim().toLocaleLowerCase().match(emailRegex));
	}

	$.validator.methods.url = function( value, element ) {
		var webRegex = /^(http:\/\/|https:\/\/)?[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
		return this.optional(element) || (!!value.trim().toLocaleLowerCase().match(webRegex));
	}

	$.validator.methods.phone = function( value, element ) {
		var phoneRegex = /^[+]*[]{0,1}[0-9]{1,4}[]{0,1}[-/0-9]*$/;
		return phoneRegex.test(value.trim());
	    return this.optional(element) || (!!value.trim().match(phoneRegex));
	}

	$.validator.methods.filesize = function( value, element, param ) {
		return this.optional(element) || (element.files[0].size <= param);
	}
	
	$.extend(jQuery.validator.messages, {
		phone: jQuery.validator.format("The telephone number is invalid."),
		text: jQuery.validator.format("Letters, Numbers, Apostrophe and Space only please"),
		filesize: jQuery.validator.format("File size must be less than {0}"),
	});

	$('form[name="rsvp-form"]').each(function() {
		$form = $(this);
	    $form.validate({
			errorElement: 'p',
			submitHandler: function(form) {
				var fd = new FormData(form);
			    var file = $('input[type="file"]');
			    $($form).addClass('submited');
            	$($form).trigger('rsvpSubmit');
			    if(file.length > 0){
				    var individual_file = file[0].files[0];
				    fd.append("file", individual_file);
			    }
			    fd.append('action', 'rsvp_submit'); 		    	
				var postXHR = $.ajax({
		            url: url, 
		            type: "POST",
		            data: fd,
		            dataType: 'json',
		            cache: false,      
		            contentType: false,
			        processData: false,
		            success: function(resp) {
		            }
		        });
				postXHR.done( function( resp ){
					if(resp.status == true || resp.message == "user already registered"){
						$(rsvpForm)[0].reset();
					}
					$('.server_resp').show();
					$('.server_resp').text(resp.message);
					if(resp.data == 1){
						refresh_captcha();
					}
					setTimeout(function(){ $('.server_resp').hide(); }, 10000);
					$($form).removeClass('submited');
					if(resp.invalid == true){
						$($form).trigger('InvalidData');
						$('.server_resp').addClass("invalid");
					}else{
						$('.server_resp').removeClass("invalid");						
					}
					if(resp.message == 'Request completed successfully!'){
						$($form).trigger('mailSent');
					}else if(resp.message == 'There was an error trying to send your message. Please try again later.'){
						$($form).trigger('mailFailed');
					}
					(resp.data == 1) ? refresh_captcha() : '';
				});
				postXHR.fail( function(resp){
					if(resp.data == 1){
						refresh_captcha();
					}
					$($form).removeClass('submited');
				});

		        return false;
			}
		});
	});

    function refresh_captcha() {
		grecaptcha.reset();
    }
});