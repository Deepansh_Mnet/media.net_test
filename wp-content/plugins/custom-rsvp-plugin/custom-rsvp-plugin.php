<?php
/** Plugin name:Custom Rsvp Plugin */

// abor if this is called 
defined( 'ABSPATH' ) or die('unahuthorized');

// require once the composer autoloads
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
	* This runs during activation
*/
function activate(){
	IncRsvp\Base\Activate::activate();
}

/**
	* This runs during deactivation
*/
function deactivate(){
	IncRsvp\Base\Deactivate::deactivate();
}

register_activation_hook(__FILE__, 'activate' );
register_deactivation_hook(__FILE__, 'deactivate' );

/**
	* Initialize all the core classes of the plugin
*/
if ( class_exists('IncRsvp\\Init')){
	IncRsvp\Init::register_services();
}